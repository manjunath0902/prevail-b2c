'use strict';

/* API includes */
var Transaction = require('dw/system/Transaction');
/**
 * E2EcyberSource Authorization, call SCMP service If cyberSource is using SCMP(Simple Commerce Message Protocol), call SOAP service otherwise.
 * call SOAP method if ete_cs_APIMethod is present and is set to SAOP in site preference,calls SCMP otherwise
 * @param {object}Order SFCC order object
 * @returns {Boolean}
 */
function start(order) {
    var cybersourceAPIMethod = 'ete_cs_APIMethod' in dw.system.Site.getCurrent().preferences.custom ? dw.system.Site.getCurrent().preferences.custom.ete_cs_APIMethod : '';
    var args = {};
    args.Basket = order;
    args.OrderNumber = order.orderNo;
    args.cvNumber = session.forms.billing.paymentMethods.creditCard.cvn.value;
    if (cybersourceAPIMethod == 'SOAP') {
        return soap(args);
    } else {
        return scmp(args);
    }
}

/**
 * Call etecybersource.scmp.payment.cyberSourceAuth service for authorization 
 * @param {object} args
 * @returns {Boolean}
 */
function soap(args) {
    var result;
    Transaction.wrap(function() {
        result = require('~/cartridge/scripts/cybersourceauth/CyberSourceAuthorizationSOAP').cyberSourceAuthorizationSOAP(args);
    });
    if (result === PIPELET_ERROR) {
        return false;
    } else {
        return true;
    }
}

/**
 * Call etecybersource.soap.payment.runTransaction service for authorization
 * @param {object} args
 * @returns {Boolean}
 */

function scmp(args) {
    var result;
    Transaction.wrap(function() {
        result = require('~/cartridge/scripts/cybersourceauth/CyberSourceAuthorization').cyberSourceAuthorization(args);
    });
    if (result === PIPELET_ERROR) {
        return false;
    } else {
        return true;
    }
}

/*Module exports*/
exports.start = start;