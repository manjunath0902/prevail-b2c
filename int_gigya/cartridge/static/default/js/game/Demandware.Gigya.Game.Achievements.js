/*******************************************************************
 * 
 *  Invokes the game mechanics achievements plugin
 * 
 ******************************************************************/

if(typeof(Demandware)=='undefined')Demandware = {};
if(!Demandware.Gigya)Demandware.Gigya = {};
if(!Demandware.Gigya.Game)Demandware.Gigya.Game = {};

Demandware.Gigya.Game.Achievements = new function(){
	var _divParams = null;
	var _width = null;
	var _height = null;
	var _additionalParameters = null;
		
	//constructor
	new function(){
		$(document).bind("ready", initialize);
		$(window).bind("unload", dispose);
	}
	
	function initialize(){
		initializeElements();
		showAchievements();
	}
	
	function initializeElements(){
		_divParams = $('#divGigyaAchievementsParams');
		_width = _divParams.find('#txtGigyaAchievementsWidth').val()||300;
		_height =  _divParams.find('#txtGigyaAchievementsHeight').val()||100;	
		if ( _divParams.find('#txtGigyaAdditionalParameters').val() !== "null") {
			_additionalParameters = $.parseJSON(_divParams.find('#txtGigyaAdditionalParameters').val());
		}
	}
	
	function showAchievements(){
		var statusParams = {
			containerID:'divGigyaAchievements'
			,width:_width
			,height:_height
			,excludeChallenges:''
		};
		if ( _divParams.find('#txtGigyaAdditionalParameters').val() !== "null") {
			for (var property in _additionalParameters) {
				statusParams[property] = _additionalParameters[property];
			}
		}
		gigya.gm.showAchievementsUI(statusParams);
	}
	
	function dispose(){
		_divParams = null;
	}
}