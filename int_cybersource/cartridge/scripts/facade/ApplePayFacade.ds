'use strict';
var Logger = require('dw/system/Logger').getLogger('Cybersource');

/**
 * This function prepares request data for Apple pay payment method
 * @param LineItemCtnrObj : dw.order.LineItemCtnr
 * @param order No  : Order No
 * @param IPAddress : clients IP address from where request is raised
 * @param data : This is encrypted payment data
 */
 
function ApplePayAPIAuthRequest(lineItemCtnr : dw.order.LineItemCtnr, orderNo : String, IPAddress : String, data)
{
	// Prepare objects from order
	var ApplePayHelper = require('int_cybersource/cartridge/scripts/helper/ApplePayHelper');
	var result = ApplePayHelper.PrepareAuthRequestObjects(lineItemCtnr);
	if(result.error){
		return result;
	}
	return ApplePayAPIObjectAuthRequest(result.billTo,result.shipTo,result.purchaseObject,result.items, orderNo, IPAddress, data);
}


/**
 * This function prepares request data for Apple pay payment method, called internally.
 * @param bill To : billing address of the order
 * @param ship to : Shipping address of the order
 * @param order No : Unique identifier of the Order object
 * @param purchaseObject : purchase Object
 * @param IPAddress : clients IP address from where request is raised
 * @param data : This is encrypted payment data
 */

function ApplePayAPIObjectAuthRequest(billTo, shipTo, purchaseObject, items, orderNo : String, IPAddress : String, data)
{
	//Objects to set in the Service Request inside facade
	var libCybersource = require('int_cybersource/cartridge/scripts/cybersource/libCybersource');
	var CybersourceHelper = libCybersource.getCybersourceHelper();	

	//**************************************************************************//
	// Set WebReference & Stub
	//**************************************************************************//	
	var csReference = webreferences.CyberSourceTransactionSI;

	var serviceRequest = new csReference.RequestMessage();
	//**************************************************************************//
	// the request object holds the input parameter for the AUTH request
	//**************************************************************************//	
	CybersourceHelper.addCCAuthRequestInfo(serviceRequest,billTo,shipTo,purchaseObject,null,orderNo, CybersourceHelper.getDigitalFingerprintEnabled(), items);
	/********************************/
	/* Apple Pay checkout-related WebService setup */
	/********************************/	
	CybersourceHelper.addApplePayAPIAuthRequestInfo(serviceRequest, orderNo, data);
	
	/********************************/
	/* DAV-related WebService setup */
	/********************************/	
	var enableDAV = CybersourceHelper.getDavEnable();
	var approveDAV = CybersourceHelper.getDavOnAddressVerificationFailure();
	
	if( enableDAV=='YES' ) {
		var ignoreDAVResult = false;
		if( approveDAV=='APPROVE' ) {
			ignoreDAVResult = true;
		}
		CybersourceHelper.addDAVRequestInfo(serviceRequest, billTo, shipTo, ignoreDAVResult);
	}
	/* End of DAV WebService setup */
	
	/* AVS Service setup */
	var ignoreAVSResult = CybersourceHelper.getAvsIgnoreResult();
	var declineAVSFlags = CybersourceHelper.getAvsDeclineFlags();
	
	CybersourceHelper.addAVSRequestInfo(serviceRequest, ignoreAVSResult, declineAVSFlags);
	/* End of AVS Service setup */
	var CardHelper = require('int_cybersource/cartridge/scripts/helper/CardHelper');
	CardHelper.writeOutDebugLog(serviceRequest,orderNo);
	
	//**************************************************************************//
	// Execute Request
	//**************************************************************************//	
	var serviceResponse = null;
	try
	{
		var dwsvc = require ("dw/svc");
		var service = dwsvc.ServiceRegistry.get("cybersource.soap.transactionprocessor.generic"); 
		serviceResponse = service.call(serviceRequest);
	}
	catch(e)
	{
		Logger.fatal("[ApplePayFacade.ds] Error in ApplePayAPIAuthRequest ( {0} )", e.message);
		return {error:true, errorMsg:e.message, errorCode:Resource.msg('cyb.applepay.errorcode.servicenotavailable', 'cybapplepay', null)};
	}
	
	if(empty(serviceResponse) || serviceResponse.status != "OK")
	{
		Logger.fatal("[ApplePayFacade.ds] ApplePayAPIAuthRequest Error : null response");
		return {error:true, errorMsg:Resource.msg('cyb.applepay.errormsg.invalidApplePayAPIAuthRequest', 'cybapplepay', null) +serviceResponse, errorCode:Resource.msg('cyb.applepay.errorcode.servicestatus', 'cybapplepay', null)};
	}
	serviceResponse = serviceResponse.object;	
	Logger.error("[ApplePayFacade.ds] ApplePayAPIAuthRequest response : "+serviceResponse);
	CardHelper.protocolResponse( serviceResponse );
	//**************************************************************************//
	// Process Response
	//**************************************************************************//		
	var result = CardHelper.ProcessCardAuthResponse(serviceResponse, shipTo, billTo);
	return {success:true, serviceResponse:result.responseObject};
}


/**
 * This function prepares authorization request data for Apple pay payment method
 * @param LineItemCtnrObj : dw.order.LineItemCtnr
 * @param order No  : Order No
 * @param IPAddress : clients IP address from where request is raised
 * @param data : This is encrypted payment data
 */

function ApplePayInAppAuthRequest(lineItemCtnr, orderNo : String, IPAddress : String, cryptogram, networkToken, tokenExpirationMonth, tokenExpirationYear, cardType)
{
	// Prepare objects from order
	var ApplePayHelper = require('int_cybersource/cartridge/scripts/helper/ApplePayHelper');
	var result = ApplePayHelper.PrepareAuthRequestObjects(lineItemCtnr);
	if(result.error){
		return result;
	}

	return ApplePayInAppObjectAuthRequest(result.billTo,result.shipTo,result.purchaseObject,result.items, orderNo, IPAddress, cryptogram, networkToken, 
		tokenExpirationMonth, tokenExpirationYear, cardType);
}

/**
 * This function prepares request data for Apple pay authorization payment method, called internally.
 * @param bill To : billing address of the order
 * @param ship to : Shipping address of the order
 * @param order No : Unique identifier of the Order object
 * @param purchaseObject : purchase Object
 * @param IPAddress : clients IP address from where request is raised
 * @param items : The items detials purchased by APple pay
 * @param cryptogram : Encrypted data
 * @param networkToken : Apple Pay token flows in network
 * @param tokenExpirationMonth : Network token expiry month
 * @param tokenExpirationYear  : Network token expiry year
 * @param cardType : Token belongs to which type of the card.
 */


function ApplePayInAppObjectAuthRequest(billTo, shipTo, purchaseObject, items, orderNo : String, IPAddress : String, cryptogram, networkToken, 
	tokenExpirationMonth, tokenExpirationYear, cardType)
{
	var libCybersource = require('int_cybersource/cartridge/scripts/cybersource/libCybersource');
	var CybersourceHelper = libCybersource.getCybersourceHelper();	
	//Objects to set in the Service Request inside facade
	var CardHelper = require('int_cybersource/cartridge/scripts/helper/CardHelper');
	var cardResult = CardHelper.CreateApplePayPaymentCardObject(networkToken, tokenExpirationMonth, tokenExpirationYear, cardType);

	//**************************************************************************//
	// Set WebReference & Stub
	//**************************************************************************//	
	var csReference = webreferences.CyberSourceTransactionSI;

	var serviceRequest = new csReference.RequestMessage();
	//**************************************************************************//
	// the request object holds the input parameter for the AUTH request
	//**************************************************************************//	
	CybersourceHelper.addCCAuthRequestInfo(serviceRequest,billTo,shipTo,purchaseObject,cardResult.card,orderNo, false, items);
	//**************************************************************************//
	// the request object holds the input parameter for the PayerAuth request
	//**************************************************************************//
	var cavv, ucafAuthenticationData, ucafCollectionIndicator, commerceIndicator, xid;
    if (cardType.equalsIgnoreCase("Visa")) {
	   cavv = cryptogram;
	   xid = cryptogram;
	   commerceIndicator = "vbv";
    } else if (cardType.equalsIgnoreCase("MasterCard")) {
	   ucafAuthenticationData = cryptogram;
	   ucafCollectionIndicator = "2";
	   commerceIndicator = "spa";
    } else if (cardType.equalsIgnoreCase("Amex")) {
	   //cavv = cryptogram.length >= 20 ? cryptogram.substring(0,20) : cryptogram;
	   //xid = cryptogram.length <= 20 ? "" : cryptogram.substring(20);
	   cavv = cryptogram;
	   xid = cryptogram;
	   commerceIndicator = "aesk";
    }
	
	CybersourceHelper.addPayerAuthReplyInfo(serviceRequest, cavv, ucafAuthenticationData, ucafCollectionIndicator, null, commerceIndicator, xid, null);
	/********************************/
	/* Apple Pay checkout-related WebService setup */
	/********************************/	
	CybersourceHelper.addApplePayInAppAuthRequestInfo(serviceRequest, orderNo);
	
	/********************************/
	/* DAV-related WebService setup */
	/********************************/	
	var enableDAV = CybersourceHelper.getDavEnable();
	var approveDAV = CybersourceHelper.getDavOnAddressVerificationFailure();
	
	if( enableDAV=='YES' ) {
		var ignoreDAVResult = false;
		if( approveDAV=='APPROVE' ) {
			ignoreDAVResult = true;
		}
		CybersourceHelper.addDAVRequestInfo(serviceRequest, billTo, shipTo, ignoreDAVResult);
	}
	/* End of DAV WebService setup */
	
	/* AVS Service setup */
	var ignoreAVSResult = CybersourceHelper.getAvsIgnoreResult();
	var declineAVSFlags = CybersourceHelper.getAvsDeclineFlags();
	
	CybersourceHelper.addAVSRequestInfo(serviceRequest, ignoreAVSResult, declineAVSFlags);
	/* End of AVS Service setup */
	CardHelper.writeOutDebugLog(serviceRequest,orderNo);
	
	//**************************************************************************//
	// Execute Request
	//**************************************************************************//	
	var serviceResponse = null;
	try
	{
		var dwsvc = require ("dw/svc");
		var service = dwsvc.ServiceRegistry.get("cybersource.soap.transactionprocessor.generic"); 
		serviceResponse = service.call(serviceRequest);
	}
	catch(e)
	{
		Logger.fatal("[ApplePayFacade.ds] Error in ApplePayInAppAuthRequest ( {0} )", e.message);
		return {error:true, errorMsg:e.message, errorCode:Resource.msg('cyb.applepay.errorcode.servicenotavailable', 'cybapplepay', null)};
	}
	
	if(empty(serviceResponse) || serviceResponse.status != "OK")
	{
		Logger.fatal("[ApplePayFacade.ds] ApplePayInAppAuthRequest Error : null response");
		return {error:true, errorMsg:Resource.msg('cyb.applepay.errormsg.invalidApplePayAPIAuthRequest', 'cybapplepay', null)+serviceResponse, errorCode:Resource.msg('cyb.applepay.errorcode.servicestatus', 'cybapplepay', null)};
	}
	serviceResponse = serviceResponse.object;	
	Logger.error("[ApplePayFacade.ds] ApplePayInAppAuthRequest response : "+serviceResponse);
	
	var CardHelper = require('int_cybersource/cartridge/scripts/helper/CardHelper');
	
	CardHelper.protocolResponse( serviceResponse );
	//**************************************************************************//
	// Process Response
	//**************************************************************************//		
	var result = CardHelper.ProcessCardAuthResponse(serviceResponse, shipTo, billTo);
	return {success:true, serviceResponse:result.responseObject};
}

module.exports = {
		ApplePayAPIAuthRequest:ApplePayAPIAuthRequest,
		ApplePayAPIObjectAuthRequest:ApplePayAPIObjectAuthRequest,
		ApplePayInAppAuthRequest:ApplePayInAppAuthRequest,
		ApplePayInAppObjectAuthRequest:ApplePayInAppObjectAuthRequest
};