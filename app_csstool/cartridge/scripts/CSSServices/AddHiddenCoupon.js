'use strict';
/**
 * Used to apply a promotion code that has been passed into Demandware as part of a CSS session.
 * This controller should be called in the Cart-Show controller before the cart-calculate that is run prior to showing the customer the cart page. 
 * When the coupon is added to the cart it is removed from the session, therefore if it is removed it will not be able to be added again by the agent.
 * Additionally, these coupon codes are inteded to be hidden from the Call Center Agent and Customer, so you should add the custom property to Coupon Line Item "cssProvidedPromotion" and check for the existence of that propety on the couponLineItem when outputting the coupon code.
 * If the value is present you should mask it with text indicating that a promotion from CSS has been applied to the order.
 */
/*API Includes*/
var Transaction = require('dw/system/Transaction');
var BasketMgr = require('dw/order/BasketMgr');

function addHiddenCoupon() {
    var basket = BasketMgr.getCurrentBasket();
    if (basket) {
        if ((('CSSPromotionCode' in session.custom) && !empty(session.custom.CSSPromotionCode))) {
            Transaction.wrap(function() {
                var couponLineItem = basket.createCouponLineItem(session.custom.CSSPromotionCode);
                if (couponLineItem) {
                    couponLineItem.custom.cssProvidedPromotion = true;
                    delete session.custom.CSSPromotionCode;
                }
            });
        }
    }
}
/*Module Exports*/
exports.addHiddenCoupon = addHiddenCoupon;