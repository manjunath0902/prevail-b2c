'use strict';

/**
 * My Account Forgot Password - Fire when user initiates forgot password procedure
 */
exports.forgotPassword = function() {
    dataLayer.push({
        'event': 'myAccountForgot', // case sensitive - use exact value
        'eventCategory': pageContext.type,
        'eventAction': 'Retrieve Password',
        'eventLabel': 'Forgot Password'
    });
};

/**
 * Product Click Event - Fire when a user clicks on a visible product in the category grid page
 */
exports.productClick = function($this) {

    var tile = $this.closest('.product-tile');
    var impression = pageData.ecommerce.impressions.filter(function(obj) {
        return obj.position == (tile.data('idx') + 1);
    })[0];

    dataLayer.push({
        'event': 'productClick',
        'eventCategory': impression.category,
        'eventAction': 'product click',
        'eventLabel': impression.list, //Product List Value ie. category name / search result
        'ecommerce': {
            'currencyCode': pageData.ecommerce.currencyCode,
            'click': {
                'actionField': {
                    'list': impression.list
                },
                'products': [{
                    'name': impression.name,
                    'id': impression.id,
                    'price': impression.price,
                    'category': impression.category,
                    'list': impression.list, // value will need to persist to the purchase event
                    'position': impression.position
                }]
            }
        }
    });
};


/**
 * Product Click from Recommendation Zone - Fire when a user clicks a recommended item in the PDP Recommendation Zone
 */
exports.productClickRecPDP = function($this) {

    var sku = $this.find('.capture-product-id').text();
    var impression = pageData.ecommerce.detail.impressions.filter(function(obj) {
        return obj.id == sku;
    })[0];

    dataLayer.push({
        'event': 'productClick',
        'productClicked': sku,
        'eventAction': 'product click',
        'eventLabel': '', //Product List Value ie. category name / search result
        'ecommerce': {
            'currencyCode': pageData.ecommerce.currencyCode,
            'click': {
                'actionField': {
                    'list': 'PDP you might also like'
                },
                'products': [{
                    'name': impression.name,
                    'id': impression.id,
                    'price': impression.price,
                    'category': impression.category,
                    'list': impression.list, // value will need to persist to the purchase event
                    'position': impression.position
                }]
            }
        }
    });
};


/**
 *  Category Page Sort - Fire when a user changes the sort by value on the category grid page
 */
exports.sort = function(sortID) {
    dataLayer.push({
        'event': 'pageSort',
        'eventCategory': pageContext.type,
        'eventAction': 'page sort',
        'eventLabel': 'category page sort: ' + sortID //Note naming convention
    });
};

/**
 *  Category Page Refinement - Fire when a user makes a category refinement selection
 */
exports.refinement = function($this) {

    if ($this.parents('.refinement').length === 0) {
        return;
    }

    var refinementType = $this.parents('.refinement').find('h3').text();
    var refinementValue = $this.text();
    dataLayer.push({
        'event': 'pageRefinement',
        'eventCategory': pageContext.type,
        'eventAction': 'category refinement',
        'eventLabel': 'category page refinement: ' + (refinementType ? refinementType.replace(/(?:\r\n|\r|\n)/g, '') : '') + ': ' + (refinementValue ? refinementValue.replace(/(?:\r\n|\r|\n)/g, '') : ''),
        'refinementType': refinementType ? refinementType.replace(/(?:\r\n|\r|\n)/g, '') : '',
        'refinementValue': refinementValue ? refinementValue.replace(/(?:\r\n|\r|\n)/g, '') : ''
    });
};

/**
 *  Add to Cart - Fires when a user adds an item to their shopping cart
 */
exports.addToCart = function($form) {
    if (!pageData.ecommerce) {
        return;
    }
    var product = pageData.ecommerce.detail.products[0];

    var productVariations = $form.find('.product-variations').length > 0 ? $form.find('.product-variations') : $('.product-variations');
    var color = '';
    var size = '';
    if (productVariations) {
        var variationAttributes = productVariations.data('attributes');
        if (variationAttributes && variationAttributes.color) {
            color = variationAttributes.color.displayValue;
        }
        if (variationAttributes && variationAttributes.size) {
            size = variationAttributes.size.displayValue;
        }
    }

    dataLayer.push({
        'event': 'addToCart',
        'productSKUAdded': $form.find('input[name="pid"]').val(), // product SKU added
        'ecommerce': {
            'currencyCode': pageData.ecommerce.currencyCode,
            'add': {
                'products': [{
                    'id': product.id,
                    'name': product.name,
                    'price': $('#product-set-list').length > 0 ? $form.find('.price-sales').text().substr(1) : product.price,
                    'category': product.category,
                    'dimension5': color, //product color
                    'dimension6': size, //product size
                    'dimension7': '', //product rating
                    'dimension8': '', //number of product reviews
                    'dimension9': $form.find('input[name="pid"]').val(), //variation product id
                    'quantity': $form.find('input[name="Quantity"]').val()
                }]
            }
        }
    });
};

/**
 *  Size Chart - When a user views the sizing chart information page on PDP
 */
exports.sizeChart = function() {
    dataLayer.push({
        'event': 'sizeChart',
        'eventCategory': pageContext.type,
        'eventAction': 'product size chart',
        'eventLabel': $('.product-col-2').find('input[name="pid"]').val() + ':' + $('.product-col-2 .product-name').text()
    });
};

/**
 * Find in Store - Fire when a user clicks "Find in Store" on PDP
 */
exports.findInStorePDP = function() {
    dataLayer.push({
        'event': 'findInStore',
        'eventCategory': pageContext.type,
        'eventAction': 'find in store - initiate',
        'eventLabel': $('.product-col-2').find('input[name="pid"]').val() + ':' + $('.product-col-2 .product-name').text()
    });
};

/**
 * Find in Store � Zip Code Search - Fire when a user searches their zip code for product availability
 */
exports.findInStoreSearchPDP = function(label) {
    dataLayer.push({
        'event': 'searchZipCode',
        'eventCategory': pageContext.type,
        'eventAction': 'find in store - search',
        'eventLabel': label
    });
};

/**
 * Find in Store - Zip Code Search - Fire when a user searches their zip code for product availability
 */
exports.bisnPDP = function() {
    dataLayer.push({
        'event': 'backInStock',
        'eventCategory': pageContext.type,
        'eventAction': 'back in stock click',
        'eventLabel': $('.product-col-2').find('input[name="pid"]').val() + ':' + $('.product-col-2 .product-name').text()
    });
};

/**
 * Newsletter subscription event - Fire event when user moves to next step of checkout process
 */
exports.newsLetterSignupCheckout = function() {
    dataLayer.push({
        'event': 'newsletterSubscriptionCheckout',
        'eventCategory': 'checkout',
        'eventAction': 'subscribe to newsletter',
        'eventLabel': $('input[name$="billing_billingAddress_addToEmailList"]').is(':checked') ? 'checked' : 'unchecked'
    });
};

/**
 * Add to Wishlist / Save for Later - Fires when a user adds an item to their wishlist
 */
exports.addToWishlistPDP = function($this) {
    if ($this.data('action') !== 'wishlist') {
        return;
    }
    dataLayer.push({
        'event': 'saveProduct',
        'eventCategory': pageContext.type,
        'eventAction': 'save for later',
        'eventLabel': $('.product-col-2').find('input[name="pid"]').val() + ':' + $('.product-col-2 .product-name').text()
    });
};

/**
 * Promotion code applied - TIMING: Fires when a user adds an item to their shopping cart
 */
exports.applyCoupon = function(code) {
    dataLayer.push({
        'event': 'promoApplied',
        'eventCategory': 'checkout',
        'eventAction': 'promotion applied',
        'eventLabel': code
    });
};

exports.init = function() {

    if (pageContext.type === 'MyAccount') {
        /**
         * My Account Check Order Status - Fire when a user checks the status of their order
         */
        $('button[name$="_ordertrack_findorder"]').click(function() {

            if (!$(this).closest('form').valid()) {
                return;
            }

            dataLayer.push({
                'event': 'myAccountCheckOrder', // case sensitive - use exact value
                'eventCategory': pageContext.type,
                'eventAction': 'Check Order Status',
                'eventLabel': 'Check Order Status'
            });
        });
    } else if (pageContext.type === 'storefront') {

        /**
         * Below is for homepage carousel & banners
         * Note - Carosuel structure may change from merchant to merchant. Below function assumed Prevail structure.
         */
        var ecommerceObj = {};
        var ecommerce = {};
        ecommerce.promoView = {};
        ecommerce.promoView.promotions = [];

        //Header banner
        $('.slotJSON').each(function() {
            $.merge(ecommerce.promoView.promotions, $(this).data('slotjson'));
        });

        //Home page slider
        $('.pt_storefront #homepage-slider #homepage-slides li img').click(function() {
            dataLayer.push({
                'event': 'promotionClick',
                'ecommerce': {
                    'promoClick': {
                        'promotions': [{
                            'name': $(this).attr('alt'),
                            'creative': $(this).attr('src') ? $(this).attr('src').substr($(this).attr('src').lastIndexOf('/') + 1) : '',
                            'position': 'Homepage Carousel ' + ($(this).index() + 1)
                        }]
                    }
                }
            });
        });

        ecommerceObj.event = 'homePagePromoView';
        ecommerceObj.ecommerce = ecommerce;
        dataLayer.push(ecommerceObj);

    } else if (pageContext.type === 'product') {

        var $pdpMain = $('#pdpMain');
        /**
         * Product Click from Recommendation Zone - Fire when a user clicks a recommended item in the PDP Recommendation Zone
         */
        $('.recommendation-item').click(function() {
            exports.productClickRecPDP($(this));
        });

        /**
         * Facebook Share - Fire when a user clicks to share an item on Facebook
         */
        $pdpMain.on('click', '.socialsharing a[data-share="facebook"]', function() {

            dataLayer.push({
                'event': 'facebookShare',
                'eventCategory': pageContext.type,
                'eventAction': 'facebook social share',
                'eventLabel': $('.product-col-2').find('input[name="pid"]').val() + ':' + $('.product-col-2 .product-name').text()
            });
        });

        /**
         * Twitter Share - Fire when a user clicks to share an item on Twitter
         */
        $pdpMain.on('click', '.socialsharing a[data-share="twitter"]', function() {

            dataLayer.push({
                'event': 'twitterShare',
                'eventCategory': pageContext.type,
                'eventAction': 'twitter social share',
                'eventLabel': $('.product-col-2').find('input[name="pid"]').val() + ':' + $('.product-col-2 .product-name').text()
            });
        });

        /**
         * Google+ Share - Fire when a user clicks to share an item on Google+
         */
        $pdpMain.on('click', '.socialsharing a[data-share="googleplus"]', function() {

            dataLayer.push({
                'event': 'googleShare',
                'eventCategory': pageContext.type,
                'eventAction': 'google+ social share',
                'eventLabel': $('.product-col-2').find('input[name="pid"]').val() + ':' + $('.product-col-2 .product-name').text()
            });
        });

        /**
         * Pinterest Share - Fire when a user clicks to share an item on Pinterest
         */
        $pdpMain.on('click', '.socialsharing a[data-share="pinterest"]', function() {

            dataLayer.push({
                'event': 'pinterestShare',
                'eventCategory': pageContext.type,
                'eventAction': 'pinterest social share',
                'eventLabel': $('.product-col-2').find('input[name="pid"]').val() + ':' + $('.product-col-2 .product-name').text()
            });
        });

        /**
         * Size Chart - When a user views the sizing chart information page on PDP
         */
        $pdpMain.on('click', '.socialsharing a[data-share="pinterest"]', function() {

            dataLayer.push({
                'event': 'pinterestShare',
                'eventCategory': pageContext.type,
                'eventAction': 'pinterest social share',
                'eventLabel': $('.product-col-2').find('input[name="pid"]').val() + ':' + $('.product-col-2 .product-name').text()
            });
        });

    } else if (pageContext.type === 'Cart') {

        /**
         * Add to Wishlist / Save for Later - Fires when a user adds an item to their wishlist from the shopping cart page
         */
        $('.add-to-wishlist').click(function() {
            dataLayer.push({
                'event': 'saveProduct',
                'eventCategory': pageContext.type,
                'eventAction': 'save for later',
                'eventLabel': $(this).parents('.cart-row:first').find('.item-details .sku .value').text()
            });
        });

        /**
         * Proceed with PayPal - Fire when a user clicks to proceed with PayPal on the shopping cart page
         */
        $('.checkout-paypal').click(function() {
            dataLayer.push({
                'event': 'checkoutPaypal',
                'eventCategory': pageContext.type,
                'eventAction': 'Checkout with Paypal'
            });
        });

        /**
         * Remove from Cart - Fire when a user removes an item, or clears all items from their shopping cart
         */
        $('.cart-row .item-quantity-details button[name$="_deleteProduct"]').click(function() {
            var sku = $(this).parents('.cart-row:first').find('.item-details .sku .value').text();
            var skuIndex = pageData.cartedSKU.indexOf(sku);

            dataLayer.push({
                'event': 'productRemove',
                'productSKURemoved': sku, // SKU of product removed
                'ecommerce': {
                    'currencyCode': pageData.ecommerce.currencyCode,
                    'remove': {
                        'products': [{
                            'id': pageData.cartedProductID[skuIndex],
                            'name': pageData.cartedProductName[skuIndex],
                            'price': pageData.cartedProductPrice[skuIndex],
                            'category': pageData.cartedProductCategory[skuIndex],
                            'dimension5': pageData.cartedProductColor[skuIndex],
                            'dimension6': pageData.cartedProductSize[skuIndex],
                            'dimension7': '',
                            'dimension8': '',
                            'dimension9': pageData.cartedVariationID[skuIndex],
                            'list': '', // value must persist to purchase
                            'quantity': pageData.cartedQuantity[skuIndex]
                        }]
                    }
                }
            });

        });

        /**
         * Promotion code applied - TIMING: Fires when a user adds an item to their shopping cart
         */
        $('button[name$="_cart_addCoupon"]').click(function() {
            dataLayer.push({
                'event': 'promoApplied',
                'eventCategory': 'Cart',
                'eventAction': 'promotion applied',
                'eventLabel': $('input[name$="_cart_couponCode"]').val()
            });
        });

        /**
         * Proceed to Checkout - Fire when a user click s "Proceed to Checkout" from shopping cart page
         */
        $('button[name$="_cart_checkoutCart"]').click(function() {
            dataLayer.push({
                'event': 'checkoutProceed',
                'eventCategory': 'Cart',
                'eventAction': 'Proceed to Checkout'
            });
        });

    } else if (pageContext.type === 'checkout') {
        /**
         * Newsletter subscription event - Fire event when user moves to next step of checkout process
         */
        $('button[name$="_billing_save"]').click(function() {
            exports.newsLetterSignupCheckout();
        });
    }
};
