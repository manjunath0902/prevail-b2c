/**
 * The following function removes the following
 * 	 single quotes
 * 	 double quotes
 * 	 registered symbol
 */
function removeQuotesAndR(str) {

	str = str.replace("&amp;#34;", "");
	str = str.replace("&amp;#39;", "");
	str = str.replace("&amp;quot;", "");
	str = str.replace("&amp;reg;", "");

	return str;
}

/** 
 * Removes amp from encoded querystring
 * due to & separators between parameters
 * 
 */
function removeAmp(urlString) {
	
	return urlString.replace("amp;", "");
}


/**
 * The following is a helper function for Coremetrics registration tagging
 * DO NOT REMOVE
 */
function cmCreateRegistrationTag(customerEmail, customerCity, customerState, customerZIP, customerCountry, attributes){
	var emailRE = new RegExp("[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}");
	var zip = null;
	
	if(emailRE.exec(customerEmail) != null){
		if(customerCountry == "US" || customerCountry == "USA" || customerCountry == "United States"){
			if(customerZIP != null){
				zip = customerZIP.substring(0,5);
			}
		}
		else{
			zip = customerZIP;
		}
		cmCreateRegistrationTag(customerEmail, customerEmail, customerCity, (customerState != null && customerState != 'null' ? customerState : ''), zip, customerCountry, attributes);
	}
}

