'use strict';

/* API includes */
var Transaction = require('dw/system/Transaction');

/* Script includes */
var UpdatePaymentTransaction = require('~/cartridge/scripts/cybersourceAFS/UpdatePaymentTransaction');
var CyberSourceAFS = require('~/cartridge/scripts/cybersourceAFS/CyberSourceAFS.ds');

function startAFS(args) {
    var templinecntr = (args.Basket == null && args.Order != null) ? args.Order : args.Basket;
    args.OrderNumber = (args.Order != null) ? templinecntr.orderNo : templinecntr.getUUID();
    args.TenderType = 'other';
    args.ExternalAvsCode = args.ExternalAvsCode ? args.ExternalAvsCode : '';
    args.ExternalCvCode = args.ExternalCvCode ? args.ExternalCvCode : '';
    args.Basket = templinecntr;
    var cyberSourceAFSResult;
    var updatePaymentTransactionResult;

    Transaction.wrap(function() {
        cyberSourceAFSResult = CyberSourceAFS.CyberSourceAFS(args);
    });

    if (cyberSourceAFSResult != PIPELET_ERROR) {
        args.CyberSourceAFSResponse = args.Response;
        Transaction.wrap(function() {
            updatePaymentTransactionResult = UpdatePaymentTransaction.UpdatePaymentTransaction(args);
        });
        if (updatePaymentTransactionResult != PIPELET_ERROR) {
            if (args.CyberSourceAFSResponse.decision.toUpperCase() == 'REJECT') {
                return false;
            }
        }
    }
    return true;
}

exports.StartAFS = startAFS;