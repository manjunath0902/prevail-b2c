/**
* 	CreateAvataxTaxationItemsObject.ds
*
*	@input Basket : dw.order.Basket
*	@output itemarray : Array
*/

var dworder		= require ("dw/order");
var dwLogger	= require ("dw/system").Logger;

var libAvatax = require("int_avatax/cartridge/scripts/avatax/libAvatax");

function execute( args : PipelineDictionary ) : Number
{
	try {
		var basket : dworder.Basket = args.Basket;		
		var items : Array = new Array();
		var AvataxHelper = new libAvatax();
		
		var defaultProductTaxCode : String = AvataxHelper.getDefaultProductTaxCode();
		var defaultShippingMethodTaxCode : String = AvataxHelper.getDefaultShippingMethodTaxCode();
		var taxIncluded : Boolean = (dworder.TaxMgr.taxationPolicy == dworder.TaxMgr.TAX_POLICY_GROSS);
		
		// Create product line items
		for each (let li : dworder.ProductLineItem in basket.productLineItems) {
			if (!empty(li.shipment.shippingAddress)) {
				let line = new AvataxHelper.taxReference.Line();
				let shippingAddress : dworder.OrderAddress = li.shipment.shippingAddress;
				line.destinationCode	= li.shipment.shippingAddress.UUID;
				line.discounted 		= false;
				line.description 		= li.productName;
				line.itemCode 			= li.productID;
				line.taxCode 			= !empty(li.taxClassID) ? li.taxClassID : defaultProductTaxCode;
				line.no 				= li.UUID;
				line.qty 				= li.quantityValue;
				line.amount 			= li.proratedPrice.value;
				line.originCode			= "0";
				line.taxIncluded 		= taxIncluded;
				
				items.push(line);
				
				for each (let oli : dworder.ProductLineItem in li.optionProductLineItems) {
					let line = new AvataxHelper.taxReference.Line();
					let shippingAddress : dworder.OrderAddress = li.shipment.shippingAddress;
					line.destinationCode	= li.shipment.shippingAddress.UUID;
					line.discounted 		= false;
					line.description 		= oli.productName;
					line.itemCode 			= oli.productID;
					line.taxCode 			= !empty(oli.taxClassID) ? oli.taxClassID : defaultProductTaxCode;
					line.no 				= oli.UUID;
					line.qty 				= oli.quantityValue;
					line.amount 			= oli.proratedPrice.value;
					line.originCode			= "0";
					line.taxIncluded 		= taxIncluded;
					
					items.push(line);
				}
				
				if (!empty(li.shippingLineItem)) {
					let line = new AvataxHelper.taxReference.Line();
					let sli : dworder.ShippingLineItem = li.shippingLineItem;
					let shippingAddress : dworder.OrderAddress = li.shipment.shippingAddress;
					line.destinationCode	= li.shipment.shippingAddress.UUID;
					line.discounted 		= false;
					line.description 		= sli.lineItemText;
					line.itemCode 			= sli.lineItemText;
					line.taxCode 			= !empty(sli.taxClassID) ? sli.taxClassID : defaultShippingMethodTaxCode;
					line.no 				= sli.UUID;
					line.qty 				= 1;
					line.amount 			= sli.adjustedPrice.value;
					line.originCode			= "0";
					line.taxIncluded 		= taxIncluded;
					
					items.push(line);
				}
			}
		}
		
		// Create shipping line items
		for each (var shipment : dworder.Shipment in basket.shipments) {
			if (!empty(shipment.shippingAddress)) {
				for each (let li : dworder.ShippingLineItem in shipment.shippingLineItems) {
					let line = new AvataxHelper.taxReference.Line();
					let shippingAddress : dworder.OrderAddress = shipment.shippingAddress;
					
					line.qty				= 1;
					line.description		= li.lineItemText;
					line.itemCode			= li.ID;
					line.taxCode			= !empty(li.taxClassID) ? li.taxClassID : defaultShippingMethodTaxCode;
					line.no					= li.getUUID();
					line.destinationCode	= shipment.shippingAddress.UUID;
					line.discounted 		= false;
					line.originCode			= "0";
					line.amount				= li.adjustedPrice.value;
					line.taxIncluded 		= taxIncluded;
					
					items.push(line);
				}
			}
		}
		
		args.itemarray = items;
		
	    return PIPELET_NEXT;
	}
	catch(e) {
		dwLogger.getLogger("Avalara", "AvaTax").error("[CreateAvataxTaxationItemsObject.ds] failed with the following error: {0}", e.message);
		return PIPELET_ERROR;
	}	    
}