'use strict';

/**
 * Model for search functionality.
 *
 * @module models/SearchModel
 */

/* API Includes */
var CatalogMgr = require('dw/catalog/CatalogMgr');
var ProductSearchModel = require('dw/catalog/ProductSearchModel');

/**
 * Search helper class providing enhanced search functionality.
 * @class module:models/SearchModel~SearchModel
 */
var SearchModel = ({

});

/**
 * Initializes the given search model using the queryStringObject.
 *
 * @param {dw.catalog.SearchModel} searchModel SearchModel to initialize.
 * @param {Object} queryStringObject queryStringObject to read common search parameters from.
 * @returns {dw.catalog.SearchModel} Search model.
 */
SearchModel.initializeSearchModel = function (searchModel, queryStringObject) {

    if (searchModel) {
        if (queryStringObject.q) {
            searchModel.setSearchPhrase(queryStringObject.q);
        }

        if (queryStringObject.psortb1 && queryStringObject.psortd1) {
            searchModel.setSortingCondition(queryStringObject.psortb1, parseInt(queryStringObject.psortd1));
        }

        if (queryStringObject.psortb2 && queryStringObject.psortd2) {
            searchModel.setSortingCondition(queryStringObject.psortb2, parseInt(queryStringObject.psortd2));
        }

        if (queryStringObject.psortb3 && queryStringObject.psortd3) {
            searchModel.setSortingCondition(queryStringObject.psortb3, parseInt(queryStringObject.psortd3));
        }

        for (var param in queryStringObject){
            if (param.indexOf('prefn') > -1) {
                searchModel.addRefinementValues(queryStringObject[param], queryStringObject['prefv'+param.substr(-1)]);
            }
        }

    }
    return searchModel;
};

/**
 * Creates and initializes a {dw.catalog.ProductSearchModel} based on the given HTTP parameters.
 *
 * @param {dw.web.queryStringObject} queryStringObject queryStringObject to read product search parameters from.
 * @returns {dw.catalog.ProductSearchModel} Created and initialized product serach model.
 */
SearchModel.initializeProductSearchModel = function (queryStringObject) {
    var productSearchModel = this.initializeSearchModel(new ProductSearchModel(), queryStringObject);

    productSearchModel.setRecursiveCategorySearch(true);

    if (queryStringObject.pid) {
        productSearchModel.setProductID(queryStringObject.pid);
    }

    if (queryStringObject.pmin) {
        productSearchModel.setPriceMin(parseInt(queryStringObject.pmin));
    }

    if (queryStringObject.pmax) {
        productSearchModel.setPriceMax(parseInt(queryStringObject.pmax));
    }

    var sortingRule = queryStringObject.srule ? CatalogMgr.getSortingRule(queryStringObject.srule) : null;
    if (sortingRule) {
        productSearchModel.setSortingRule(sortingRule);
    }

    // only add category to search model if the category is online
    if (queryStringObject.cgid) {
        var category = CatalogMgr.getCategory(queryStringObject.cgid);
        if (category && category.isOnline() && productSearchModel) {
            productSearchModel.setCategoryID(category.getID());
        }

    }

    return productSearchModel;
};

module.exports = SearchModel;
