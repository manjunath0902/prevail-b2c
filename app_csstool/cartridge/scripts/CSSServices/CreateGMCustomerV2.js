'use strict';


/**
 * Used to create a customer stub in GlobalMerchant.
 * This pipeline should be called after a new user registration to get the basic information added to the GlobalMerchant database.
 *
 * This pipeline is dependent upon the following site preferences:
 *
 * CSSServiceHostName
 * CSSServiceUserURL
 * CSSServicePrivateKey
 * CSSServiceClientID
 *
 * This typically only needs to be called from the Account-CreateAccount pipeline, but if there are additional places that a customer can create an account,
 * such as during checkout that call a different registration pipeline, ensure that this pipeline is called from there as well.
 */

/*API Includes*/
var Transaction = require('dw/system/Transaction');

/**
 * @returns {dw.system.Session.getCustomer()}, the customer associated with the current storefront session.
 */
function createGMCustomerV2() {

    var customer = session.getCustomer();
    Transaction.wrap(function() {
        var createGMCustomerV2 = require('app_csstool/cartridge/scripts/gmfunctions/CreateGMCustomerV2');
        var result = createGMCustomerV2.createGMCustomer(customer);
        return result;
    });
}

/*Module Exports*/
exports.createGMCustomerV2 = createGMCustomerV2;