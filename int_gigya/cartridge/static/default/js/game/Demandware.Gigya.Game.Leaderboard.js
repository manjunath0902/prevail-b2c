/*******************************************************************
 * 
 *  Invokes the game mechanics leaderboard plugin
 * 
 ******************************************************************/

if(typeof(Demandware)=='undefined')Demandware = {};
if(!Demandware.Gigya)Demandware.Gigya = {};
if(!Demandware.Gigya.Game)Demandware.Gigya.Game = {};

Demandware.Gigya.Game.Leaderboard = new function(){
	var _divParams = null;
	var _width = null;
	var _challenge = null;
	var _period = null;
	var _count = null;
	var _additionalParameters = null;
		
	//constructor
	new function(){
		$(document).bind("ready", initialize);
		$(window).bind("unload", dispose);
	}
	
	function initialize(){
		initializeElements();
		showUserStatus();
	}
	
	function initializeElements(){
		_divParams = $('#divGigyaLeaderboardParams');
		_width = _divParams.find('#txtGigyaLeaderboardWidth').val()||300;
		_challenge =  _divParams.find('#txtGigyaLeaderboardChallenge').val()||'_default';
		_period =  _divParams.find('#txtGigyaLeaderboardPeriod').val()||'7days';
		_count =  _divParams.find('#txtGigyaLeaderboardCount').val()||12;
		if ( _divParams.find('#txtGigyaAdditionalParameters').val() !== "null") {
			_additionalParameters = $.parseJSON(_divParams.find('#txtGigyaAdditionalParameters').val());
		}
	}
	
	function showUserStatus(){
		var leaderboardParams = {
			containerID:'divGigyaLeaderboard'
			,width:_width
			,challenge:_challenge
			,period:_period
			,totalCount:_count
		};
		
		if ( _divParams.find('#txtGigyaAdditionalParameters').val() !== "null") {
			for (var property in _additionalParameters) {
				leaderboardParams[property] = _additionalParameters[property];
			}
		}
		
		gigya.gm.showLeaderboardUI(leaderboardParams);
	}
	
	function dispose(){
		_divParams = null;
	}
}