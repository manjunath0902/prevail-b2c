/*******************************************************************
 * 
 *  Invokes the Share plugin
 * 
 ******************************************************************/

if(typeof(Demandware)=='undefined')Demandware = {};
if(!Demandware.Gigya)Demandware.Gigya = {};

Demandware.Gigya.Share = new function(){
	var _this = this;
	var _divParams = null;
	var _btnShare = null;
	var _title = null;
	var _description = null;
	var _imageUrl = null;
	var _linkUrl = null;
	var _providers = null;
	var _shareActionId = null;
	var _shareMessage = null;
	var _shareWithFeed = null;
	var _showAlways = null;
	var _showNever = null;
	var _additionalParameters = null;
	
	
	_this.autoShare = false;

	//constructor
	new function(){
		$(document).bind("ready", initialize);
		$(window).bind("unload", dispose);
	}
	
	function initialize(){
		initializeElements();
		_btnShare.bind('click',btnShare_OnClick);
		if(_this.autoShare)
			btnShare_OnClick(null);
		else
			_btnShare.css('visibility','visible');
	}
	
	function initializeElements(){
		_btnShare = $('#btnGigyaShare');
		_divParams = $('#divGigyaShareParams');
		_title = _divParams.find('#txtGigyaShareTitle').val();
		_description = _divParams.find('#txtGigyaShareDescription').val();
		_imageUrl = _divParams.find('#txtGigyaShareImageUrl').val();
		_linkUrl = _divParams.find('#txtGigyaShareLinkUrl').val();
		_providers = _divParams.find('#txtGigyaShareProviders').val();
		_shareActionId = _divParams.find('#txtGigyaShareActionId').val();
		_shareMessage = _divParams.find('#txtGigyaShareActionMessage').val();
		_shareWithFeed =  _divParams.find('#txtGigyaShareWithFeed').val()!='false';
		_showAlways = _divParams.find('#txtGigyaShareAlways').val()||'unchecked';
		_showNever = _divParams.find('#txtGigyaShareNever').val()||false;
		if ( _divParams.find('#txtGigyaAdditionalParameters').val() !== "null") {
			__additionalParameters = $.parseJSON(_divParams.find('#txtGigyaAdditionalParameters').val());
		}
	}
	
	function displayError(event){
		 alert('An error has occured while trying to share' + ': ' + event.errorCode + '; ' + event.errorMessage);
	}
	
	function configureAutoSharing(params){
		if(_shareActionId){
			params.showAlwaysShare=_showAlways;
			params.showNeverShare=_showNever;
			params.autoShareActionID=_shareActionId;
		}
		
		return params;	
	}
	
	function dispose(){
		if(_btnShare)
			_btnShare.unbind('click',btnShare_OnClick);
		_this = _divParams = _btnShare = null;
	}
	
	function btnShare_OnClick(evt){
		var shareAction = new gigya.services.socialize.UserAction();
		shareAction.setTitle(_title);
		shareAction.setDescription(_description);
		shareAction.setLinkBack(_linkUrl);
		shareAction.setActionName(_shareMessage);
		shareAction.addMediaItem( { type: "image", src: _imageUrl, href: _linkUrl }); 

        var params=
	    {
        		userAction: shareAction //the gigya user action to post
        		,context: shareAction //object to be referenced after event
        		,showMoreButton: true    //show more button to allow paging to additional providers
        		,showEmailButton: true  //show email button 
        		,enabledProviders:_providers //specify social network providers 
        		,useHTML: true //use html version of plugin (rather than flash)
        		,scope:_shareWithFeed?'both':'external'
    			,privacy:Demandware.Gigya.PrivacyLevel
        		//,onError: displayError   // error callback
        }
        if ( _divParams.find('#txtGigyaAdditionalParameters').val() !== "null") {
			for (var property in _additionalParameters) {
			    params[property] = _additionalParameters[property];
			}
		}
        
        params = configureAutoSharing(params);
    
        gigya.services.socialize.showShareUI(params);
    }
}