'use strict';

/* API Includes */
var Cart = require('~/cartridge/scripts/models/CartModel');
var PaymentMgr = require('dw/order/PaymentMgr');
var Transaction = require('dw/system/Transaction');
var PaymentInstrument = require('dw/order/PaymentInstrument');

/* Script Modules */
var app = require('~/cartridge/scripts/app');

/**
 * Verifies a credit card against a valid card number and expiration date and possibly invalidates invalid form fields.
 * If the verification was successful a credit card payment instrument is created.
 */
function Handle(args) {
    var cart = Cart.get(args.Basket);
    var creditCardForm = app.getForm('billing.paymentMethods.creditCard');
    var PaymentMgr = require('dw/order/PaymentMgr');
    Transaction.wrap(function () {
    	cart.removePaymentInstruments(cart.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD));
        cart.removePaymentInstruments(cart.getPaymentInstruments(PaymentInstrument.METHOD_BML));
        cart.removePaymentInstruments(cart.getPaymentInstruments('PayPal'));
        cart.removeExistingPaymentInstruments('PayWithAmazon');
        var paymentInstrument = cart.createPaymentInstrument('PayWithAmazon', cart.getNonGiftCertificateAmount());
    });

    return {success: true};
}

/**
 * Authorizes a payment using a credit card. The payment is authorized by using the PAY_WITH_AMAZON processor
 * only and setting the order no as the transaction ID. Customizations may use other processors and custom
 * logic to authorize credit card payment.
 */
function Authorize(args) {
    var orderNo = args.OrderNo;
    var paymentInstrument = args.PaymentInstrument;
    var paymentProcessor = PaymentMgr.getPaymentMethod(paymentInstrument.getPaymentMethod()).getPaymentProcessor();

    Transaction.wrap(function () {
        paymentInstrument.paymentTransaction.transactionID = orderNo;
        paymentInstrument.paymentTransaction.paymentProcessor = paymentProcessor;
    });
    
    //Authorization call
    var responseResult = require('int_amazonpayments/cartridge/controllers/AmazonPayment').confirmOrderReference();
    var httpClient = responseResult.object;
    
    if(httpClient && httpClient.statusCode == 200) {
    	var result = new XML(httpClient.text);
		var ns = new Namespace("http://mws.amazonservices.com/schema/OffAmazonPayments/2013-01-01");
		
		//Read requestId from response and store it in paymentInstrument.
		Transaction.wrap(function() {
			paymentInstrument.custom.apRequestId = result.ns::ResponseMetadata.ns::RequestId;
			paymentInstrument.custom.apOrderReferenceId = session.privacy.amazonOrderReferenceId;
		});
    	return {authorized: true};
    } else {
    	return {error: true};
    }
}

/*
 * Module exports
 */

/*
 * Local methods
 */
exports.Handle = Handle;
exports.Authorize = Authorize;
