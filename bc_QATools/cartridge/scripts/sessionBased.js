'use strict';

/**
 * SessionBased
 */
/*Script Includes*/
var app = require('app_adapter/cartridge/scripts/app');

/* API includes */
var BasketMgr = require('dw/order/BasketMgr');
var Resource = require('dw/web/Resource');
var OrderMgr = require('dw/order/OrderMgr');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');

function reviewSummary(context) {

    var basket = BasketMgr.getCurrentOrNewBasket();
    if (basket !== null) {
        var pageMeta = require('app_adapter/cartridge/scripts/meta');
        var viewContext = require('app_prevail_core/cartridge/scripts/common/extend').immutable(context, {
            Basket: basket
        });
        pageMeta.update({
            pageTitle: Resource.msg('summary.meta.pagetitle', 'checkout', 'SiteGenesis Checkout')
        });
        app.getView(viewContext).render('checkout/summary/summary');
    }
}

function loadOrder() {

    var orderNo = request.httpParameterMap.orderId.stringValue;
    if (!empty(orderNo)) {
        var order = OrderMgr.getOrder(orderNo);
        Transaction.wrap(function() {
            var basket = BasketMgr.createBasketFromOrder(order);
            var currentBasket = BasketMgr.getCurrentOrNewBasket();
            currentBasket.createBillingAddress();
            var args = {
                Basket: basket,
                CurrentBasket: currentBasket
            };

            var copyBasket = require('bc_QATools/cartridge/scripts/CopyBasket');
            copyBasket.copyBasket(args);

            var cart = app.getModel('Cart').get();
            cart.calculate();
        });
    }
    var url = request.httpParameterMap.location.value;
    var Location = URLUtils.https(url, 'from', request.httpParameterMap.from.value, 'click', request.httpParameterMap.click.value, 'hover', request.httpParameterMap.hover.value, 'orderNo', request.httpParameterMap.orderNo.value, 'AddressID', request.httpParameterMap.AddressID.value)

    response.redirect(Location);
}

function register() {

    if (request.httpParameterMap.login.value) {
        Transaction.wrap(function() {
            dw.customer.CustomerMgr.loginCustomer(request.httpParameterMap.login.value, request.httpParameterMap.password.value, false);
        });
        loadOrder();
    }
}

exports.ReviewSummary = reviewSummary;
exports.LoadOrder = loadOrder;
exports.Register = register;