/**
* This script will put a file specified by the input parameter "File" to a FTP server.
*
*   -@input ExampleIn : String This is a sample comment.
*   @input File : dw.io.File The file to put on the FTP server.
*   @input Hostname : String The hostname of the FTP server.
*   @input Path : String The path on the FTP server.
*   @input FileName : String The file name on the FTP server.
*   @input Username : String The username for the FTP server.
*   @input Password : String The password for the FTP server.
*   @output ErrorMessage1 : String If an error occures this will hold information about the error
*
*/
importPackage( dw.system );
importPackage( dw.io );
importPackage( dw.net );

function execute( pdict : PipelineDictionary ) : Number
{
	
	var file : File = pdict.File;
	if (file == null) {
		Logger.error("SendCDFFile.ds: No file to send given");
		pdict.ErrorMessage1 = "SendCDFFile.ds: No file to send";
		return PIPELET_ERROR;
    }
    
    var hostname : String = pdict.Hostname;
    if (empty(hostname)) {
    	Logger.error("SendCDFFile.ds: No hostname given");
    	pdict.ErrorMessage1 = "SendCDFFile.ds: No hostname given";
		return PIPELET_ERROR;
    }
    
    var path : String = pdict.Path;
    if (empty(path))
    	path = "";
    
    var filename : String = pdict.FileName;
    if (empty(filename)) {
    	Logger.error("SendCDFFile.ds: No file name given");
    	pdict.ErrorMessage1 = "SendCDFFile.ds: No file name given";
		return PIPELET_ERROR;
    }
    
    var username : String = pdict.Username;
    if (empty(username)) {
    	Logger.error("SendCDFFile.ds: No user name given");
    	pdict.ErrorMessage1 = "SendCDFFile.ds: No user name given";
		return PIPELET_ERROR;
    }
    
    var password : String = pdict.Password;
    if (empty(password)) {
    	Logger.error("SendCDFFile.ds: No password given");
    	pdict.ErrorMessage1 = "SendCDFFile.ds: No password given";
		return PIPELET_ERROR;
    }
    
    try {
	    var ftpClient : FTPClient = new FTPClient();
	    ftpClient.setTimeout(10000);
	  	if (ftpClient.connect(hostname, username, password) && ftpClient.connected) {	
	    	var isUploaded = ftpClient.putBinary(path + file.name, file);
	    	if (!isUploaded) {
	    		Logger.error("SendCDFFile.ds: Unable to put file to \"{0}\"", hostname);
		    	pdict.ErrorMessage1 = "SendCDFFile.ds: Unable to put file to " + hostname;
				return PIPELET_ERROR;
	    	}	    	
	  	} 
	  	else {
	    	Logger.error("SendCDFFile.ds: Can not establish ftp connection to {0}!", hostname);
 		  	pdict.ErrorMessage1 = "SendDIPFile.ds: Can not establish ftp connection to {0}!" + hostname;
 	    	return PIPELET_ERROR;
	  	}
	    
	    ftpClient.disconnect();
	    Logger.debug("SendCDFFile.ds: Disconnected from host \"{0}\"", hostname);
	    
    } catch (e) {
    	Logger.error("SendCDFFile.ds: There was an error sending the CDF file. File name: \"{0}\", error message: {1}", file.getName(), e.message);
    	pdict.ErrorMessage1 = "SendCDFFile.ds: There was an error sending the CDF file. File name: \"" + file.getName() + "\", error message: " + e.message;
    	return PIPELET_ERROR;
    }
    
    return PIPELET_NEXT;
}
