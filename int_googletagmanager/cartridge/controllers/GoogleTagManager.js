'use strict';

/**
 * Controller for Google Tag Manager
 *
 * @module controllers/GoogleTagManager
 */

var app = require('app_adapter/cartridge/scripts/app');
var guard = require('app_adapter/cartridge/scripts/guard');

/**
 * Prepare and render GTM global data.
 */
function globalData() {
    var globalData = require('~/cartridge/scripts/datalayer/DataLayer.js').getGlobalData(request.httpParameterMap);
    app.getView({
        GlobalData: globalData
    }).render('googletagmanager/gtmglobaldata');
}

/** Render global data
 * @see module:controllers/GoogleTagManager~globalData */
exports.GlobalData = guard.ensure(['get'], globalData);
