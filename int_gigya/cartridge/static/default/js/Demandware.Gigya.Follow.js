/*******************************************************************
 * 
 *  Invokes the Follow Bar plugin
 * 
 ******************************************************************/

if(typeof(Demandware)=='undefined')Demandware = {};
if(!Demandware.Gigya)Demandware.Gigya = {};

Demandware.Gigya.Follow = new function(){
	var _divParams = null;
	var _providers = null;
	var _layout = null;
	var _iconSize = null;
	var _additionalParameters = null;
	
	//constructor
	new function(){
		$(document).bind("ready", initialize);
		$(window).bind("unload", dispose);
	}
	
	function initialize(){
		initializeElements();
		showFollowBar();
	}
	
	function initializeElements(){
		_divParams = $('#divGigyaFollowParams');
		_layout = _divParams.find('#txtGigyaFollowLayout').val()||'horizontal';
		_providers =  eval(_divParams.find('#txtGigyaFollowProvidersJSON').val());
		_iconSize = _divParams.find('#txtGigyaFollowIconSize').val()||32;
		if ( _divParams.find('#txtGigyaAdditionalParameters').val() !== "null") {
			_additionalParameters = $.parseJSON(_divParams.find('#txtGigyaAdditionalParameters').val());
		}
	}
	
	function showFollowBar(){
		var followBarParams = {
			containerID:'divGigyaFollow'
			,buttons:_providers
			,iconSize:_iconSize
			,layout:_layout
		};
		if ( _divParams.find('#txtGigyaAdditionalParameters').val() !== "null") {
			for (var property in _additionalParameters) {
				followBarParams[property] = _additionalParameters[property];
			}
		}

		gigya.services.socialize.showFollowBarUI(followBarParams);
		
	}
	
	function dispose(){
		_divParams = null;
	}
}