'use strict';

/* Script includes */
var guard = require('app_adapter/cartridge/scripts/guard');
var app = require('app_adapter/cartridge/scripts/app');

/*API includes*/
var Transaction = require('dw/system/Transaction');

/*Get Amazon helper*/
var AmazonHelper = require('~/cartridge/scripts/amazon/libAmazonPayments.ds').getAmazonPaymentsHelper();

function getOrderReferenceDetails() {
    var result;
    var ns = new Namespace("http://mws.amazonservices.com/schema/OffAmazonPayments/2013-01-01");
    //Make a call to Amazon - getOrderReferenceDetails
    var getOrderReferenceDetailsResponse = AmazonHelper.getOrderReferenceDetails();

    var httpClient = getOrderReferenceDetailsResponse.object;

    //If Success - Create shipping and billing addresses in basket. Also update shipping and billing forms with the address available in basket.
    if (httpClient && httpClient.statusCode == 200) {

        result = new XML(httpClient.text);
        var cart = app.getModel('Cart').get();

        //Read address from response.
        var address = {};
        address.fullName = result.ns::GetOrderReferenceDetailsResult.ns::OrderReferenceDetails.ns::Destination.ns::PhysicalDestination.ns::Name.toString();
        address.address1 = result.ns::GetOrderReferenceDetailsResult.ns::OrderReferenceDetails.ns::Destination.ns::PhysicalDestination.ns::AddressLine1.toString();
        address.city = result.ns::GetOrderReferenceDetailsResult.ns::OrderReferenceDetails.ns::Destination.ns::PhysicalDestination.ns::City.toString();
        address.stateCode = result.ns::GetOrderReferenceDetailsResult.ns::OrderReferenceDetails.ns::Destination.ns::PhysicalDestination.ns::StateOrRegion.toString();
        address.country = result.ns::GetOrderReferenceDetailsResult.ns::OrderReferenceDetails.ns::Destination.ns::PhysicalDestination.ns::CountryCode.toString();
        address.postal = result.ns::GetOrderReferenceDetailsResult.ns::OrderReferenceDetails.ns::Destination.ns::PhysicalDestination.ns::PostalCode.toString();
        address.phone = result.ns::GetOrderReferenceDetailsResult.ns::OrderReferenceDetails.ns::Destination.ns::PhysicalDestination.ns::Phone.toString();
        address.email = result.ns::GetOrderReferenceDetailsResult.ns::OrderReferenceDetails.ns::Buyer.ns::Email.toString();
        address.buyerName = result.ns::GetOrderReferenceDetailsResult.ns::OrderReferenceDetails.ns::Buyer.ns::Name.toString();

        //Separate firstName and lastName.
        address.firstName = address.fullName.split(' ')[0];
        address.lastName = (address.fullName.split(' ')[1])?address.fullName.split(' ')[1]:address.fullName.split(' ')[0];

        //Create shipping and billing addresses.
        setShippingAndBillingAddress(cart, address);

        //Update shipping form with shipping address
        app.getForm('singleshipping.shippingAddress.addressFields').copyFrom(cart.getDefaultShipment().getShippingAddress());
        app.getForm('singleshipping.shippingAddress.addressFields.states').copyFrom(cart.getDefaultShipment().getShippingAddress());

        //Update billing form with billing address
        app.getForm('billing.billingAddress.addressFields').copyFrom(cart.getBillingAddress());
        app.getForm('billing.billingAddress.addressFields.states').copyFrom(cart.getBillingAddress());

        //Store Amazon Order Reference Id in session for further use.
        session.privacy.amazonOrderReferenceId = request.httpParameterMap.orderReference.value;

        //Render address as JSON string. 
        app.getView({
            obj: address,
            args: {}
        }).render('util/json');

    } else {
        result = new XML(getOrderReferenceDetailsResponse.errorMessage);
        var errorMessage = result.ns::Error.ns::Message.toString();
      //Render errorMessage as JSON string.
        app.getView({
            obj: {'success':false, 'errorMessage': errorMessage},
            args:{}
        }).render('util/json');
    }
    return;
}

function setOrderReferenceDetails() {
    var result;
    var ns = new Namespace("http://mws.amazonservices.com/schema/OffAmazonPayments/2013-01-01");
    //Make a call to Amazon - setOrderReferenceDetails
    var setOrderReferenceDetailsResponse = AmazonHelper.setOrderReferenceDetails();

    var httpClient = setOrderReferenceDetailsResponse.object;

    if (httpClient && httpClient.statusCode == 200) {
        app.getView().render('util/successjson');
    } else {
        result = new XML(setOrderReferenceDetailsResponse.errorMessage);
        var errorMessage = result.ns::Error.ns::Message.toString();
      //Render errorMessage as JSON string
        app.getView({
            obj: {'success':false, 'errorMessage': errorMessage},
            args:{}
        }).render('util/json');
    }
}

function confirmOrderReference() {
    var confirmOrderReferenceResponse = AmazonHelper.confirmOrderReference();
    return confirmOrderReferenceResponse;
}

function setShippingAndBillingAddress(cart, address) {
    Transaction.wrap(function() {

        //Create shipping address
        var shippingAddress = cart.createShipmentShippingAddress(cart.getDefaultShipment().getID());
        shippingAddress.setFirstName(address.firstName);
        shippingAddress.setLastName(address.lastName);
        shippingAddress.setAddress1(address.address1);
        shippingAddress.setAddress2('');
        shippingAddress.setCity(address.city);
        shippingAddress.setPostalCode(address.postal);
        shippingAddress.setStateCode(address.stateCode);
        shippingAddress.setCountryCode(address.country);
        shippingAddress.setPhone(address.phone);

        //Create billing address
        var billingAddress = cart.createBillingAddress();
        billingAddress.setFirstName(address.firstName);
        billingAddress.setLastName(address.lastName);
        billingAddress.setAddress1(address.address1);
        billingAddress.setAddress2('');
        billingAddress.setCity(address.city);
        billingAddress.setPostalCode(address.postal);
        billingAddress.setStateCode(address.stateCode);
        billingAddress.setCountryCode(address.country);
        billingAddress.setPhone(address.phone);

        //setting customer name and email to basket
        cart.setCustomerEmail(address.email);
        cart.setCustomerName(address.buyerName);
    });
    return;
}

//clear session before making a service call for the firstTime
function initAmazonCheckout(){
	delete session.privacy.orderNo;
	delete session.privacy.amazonOrderReferenceId;
	delete session.privacy.accessToken;
	if(request.httpParameterMap.access_token.value) {
		session.privacy.isInAmazonPayment = true;
		session.privacy.accessToken = request.httpParameterMap.access_token.value;
	} else {
		delete session.privacy.isInAmazonPayment;
	}
	return;
}

exports.getOrderReferenceDetails = guard.ensure(['https', 'get'], getOrderReferenceDetails);
exports.setOrderReferenceDetails = guard.ensure(['https', 'get'], setOrderReferenceDetails);
exports.confirmOrderReference = confirmOrderReference;
exports.initAmazonCheckout = initAmazonCheckout;
