'use strict';

/**
 * Order history for logged in user 
 * Returns combination of JDE and SFCC order objects. If order exists in JDE then take order details from JDE SFCC otherwise.
 * @param {Object} args
 * @returns {Boolean}
 */
function show(args) {
    var orderSearchRequestResult = require('~/cartridge/scripts/orderhistory/OrderSearchRequest').orderSearchRequest(args);
    if (orderSearchRequestResult === PIPELET_ERROR) {
        return false;
    } else {
        return true;
    }
}

/**
 * Order details for logged in user
 * @param {Object} order JDE order object.
 * @returns {Object} args
 */
function detail(order) {
    var args = {};
    args.OrderNumber = order.OrderNumber;
    args.OrderType = order.OrderType;
    args.ReferenceNumber = order.Reference1;
    require('~/cartridge/scripts/orderhistory/OrderSummaryRequest').orderSummaryRequest(args);
    return args;
}

/**
 * Order details for guest user
 * @param {Object} args
 * @returns {Boolean}
 */
function orderDetailsGuest(args) {
    args.DWOrderNo = session.forms.ordertrack.orderNumber.value;
    args.EmailAddress = session.forms.ordertrack.orderEmail.value;
    args.PostalCode = session.forms.ordertrack.postalCode.value;
    var OrderDetailGuestResult = require('~/cartridge/scripts/orderhistory/OrderDetailGuest').orderDetailGuest(args);
    if (OrderDetailGuestResult === PIPELET_ERROR) {
        return false;
    } else {
        return true;
    }
}

/*Module exports*/
exports.show = show;
exports.detail = detail;
exports.orderDetailsGuest = orderDetailsGuest;
