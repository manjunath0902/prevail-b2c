'use strict';

/*API Includes*/
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var StringUtils = require('dw/util/StringUtils');
var Resource = require('dw/web/Resource');

/* Script Modules */
var Transaction = require('dw/system/Transaction');

/**
 * Gets CustomObject with giftCertificateID and details of a gift certificate including balance as JSON response.
 * 
 */
function GCBalanceCheck() {
    var giftCertificate = CustomObjectMgr.getCustomObject('GCCustom', request.httpParameterMap.giftCertificateID.value);
    var responseUtils = require('app_adapter/cartridge/scripts/util/Response');
    if (giftCertificate && giftCertificate.getCustom().enable && giftCertificate.getCustom().pin === request.httpParameterMap.gcPin.value) {
        responseUtils.renderJSON({
            giftCertificate: {
                ID: giftCertificate.custom.code,
                balance: StringUtils.formatMoney(new dw.value.Money(giftCertificate.custom.availableAmount, session.getCurrency().getCurrencyCode()))
            }
        });
    } else {
        responseUtils.renderJSON({
            error: Resource.msg('billing.giftcertinvalid', 'checkout', null)
        });
    }
}

/**
 * Redeems a gift certificate. If the gift certificate was not successfully
 * redeemed, the form field is invalidated with the appropriate error message.
 * If the gift certificate was redeemed, the form gets cleared.
 * @param args
 */
function redeemGiftCertificate(args) {


    args.gcPin = request.httpParameterMap.gcPin.stringValue || request.httpParameterMap.dwfrm_billing_giftCertPin.stringValue;
    args.giftCertificateID = request.httpParameterMap.giftCertCode.stringValue || request.httpParameterMap.dwfrm_billing_giftCertCode.stringValue;
    var giftCertError = null;
    var getGC = require('int_giftcard/cartridge/scripts/giftcard/GetGC');
    var getGCResult = getGC.getGiftCertificate(args);

    if (getGCResult) {

        var createGCPaymentInstrument = require('~/cartridge/scripts/giftcard/CreateGCPaymentInstrument');
        var createGCPaymentResult;
        Transaction.wrap(function() {
            createGCPaymentResult = createGCPaymentInstrument.createGCPayment(args);
        });
        if (createGCPaymentResult) {
            var removeOrUpdateOtherpaymentInstruments = require('~/cartridge/scripts/giftcard/RemoveOrUpdateOtherPaymentInstruments');
            var removeOrUpdatePaymentResult;
            Transaction.wrap(function() {
                removeOrUpdatePaymentResult = removeOrUpdateOtherpaymentInstruments.removeOrUpdatePayment(args);
            });

            if (removeOrUpdatePaymentResult) {
                session.forms.billinggiftcert.clearFormElement();
                giftCertError = null;
            } else {
                giftCertError = 'invalid_giftcert';
            }

        } else {
            giftCertError = 'invalid_giftcert';
        }

    } else {
        giftCertError = 'invalid_giftcert';
    }

    args.giftCertError = giftCertError;
}

/*Module Exports*/
/** Gets CustomObject and checks remaining balance available in the gift certificate.
 * @see module:int_giftcard/COGiftCard~GCBalanceCheck */
exports.GCBalanceCheck = GCBalanceCheck;
/** Redeems gift certificates.
 * @see module:int_giftcard/COGiftCard~redeemGiftCertificate */
exports.redeemGiftCertificate = redeemGiftCertificate;
