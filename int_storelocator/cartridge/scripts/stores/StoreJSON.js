/**
 * @param {dw.util.ArrayList} stores store search result
 * @returns {string} Store data in JSON string format
 */
var getStoreJSONData = function(stores) {
    var store, storeObj = [],
        storeJSONObj = {};
    for (var i = 0; i < stores.length; i++) {
        store = stores[i];
        storeJSONObj = {};
        storeJSONObj.name = store.name;
        storeJSONObj.city = store.city;
        storeJSONObj.latitude = store.latitude;
        storeJSONObj.longitude = store.longitude;
        storeJSONObj.address1 = store.address1;
        storeJSONObj.stateCode = store.stateCode;
        storeJSONObj.postalCode = store.postalCode;
        storeJSONObj.phone = store.phone;
        storeJSONObj.email = store.email;
        storeJSONObj.countryCode = store.countryCode ? store.countryCode.toString() : '';
        storeObj.push(storeJSONObj);
    }
    return JSON.stringify(storeObj);
};

exports.getStoreJSONData = getStoreJSONData;
