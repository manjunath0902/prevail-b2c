'use strict';


/**
 * Does RealTimeInventory Check
 */

/*API Includes*/
var Transaction = require('dw/system/Transaction');
var BasketMgr = require('dw/order/BasketMgr');
var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');

/**
 * @param {dw.util.ArrayList} productIDs List if product id's eligible for real-time inventory check.
 */
function checkJDEInventory(args) {
    var productAvailabilityRequest = require('int_inventorycheck/cartridge/scripts/pipelets/ProductAvailabilityRequest');
    var result = productAvailabilityRequest.checkJDEInventory(args);
    return result;
}

/**
 * @returns {Boolean} QtyNotAvailable returns true if atleast one line item does not have enough inventory.  
 */
function checkDWInventory() {
    var QtyNotAvailable = false;
    var basket = BasketMgr.getCurrentBasket();
    var plis = basket.getProductLineItems();
    var pli;
    for (var i = 0; i < plis.length; i++) {
        pli = plis[i];
        if (!QtyNotAvailable) {
            if (ProductInventoryMgr.getInventoryList().getRecord(pli.productID).ATS.value < pli.quantity.value) {
                QtyNotAvailable = true;
            }
        }
    }

    return QtyNotAvailable;
}

/**
 * @returns {Boolean} return true if items are available in JDE, false otherwise.
 */
function check() {

    if (checkDWInventory()) {
        return false;
    }

    var getProductIDs = require('int_inventorycheck/cartridge/scripts/GetProductIDs');

    var args = {
        Basket: BasketMgr.getCurrentBasket()
    };

    var productIDsResult = getProductIDs.getProductIDs(args);

    if (productIDsResult === PIPELET_ERROR) {
        return true;
    }

    var JDEInventoryResult = checkJDEInventory(args);

    if (JDEInventoryResult === PIPELET_ERROR) {
        return true;
    }

    Transaction.wrap(function() {
        var readProductAvailabilityResponse = require('int_inventorycheck/cartridge/scripts/ReadProductsAvailabilityResponse');
        readProductAvailabilityResponse.readProductAvailabilityResponse(args);
    });

    if (!args.InventoryRecordUpdated) {
        return true;
    } else {
        return false;
    }
}

/*Module Exports*/
exports.check = check;