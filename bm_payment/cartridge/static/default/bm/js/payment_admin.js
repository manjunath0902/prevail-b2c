String.prototype.format = function() {
    var formatted = this;
    for (var i = 0; i < arguments.length; i++) {
        var regexp = new RegExp('\\{'+i+'\\}', 'gi');
        formatted = formatted.replace(regexp, arguments[i]);
    }
    return formatted;
};

(function($){
$(document).ready(function() {
	var paginationHandler = function(e) {
		var form = $(e.target);
		var submit = $(':input[type=submit]:focus');
		var tab = form.parents('div[id=bm-payment-orders-list]');

		var pageSize = submit.attr('id') == 'PageSize' ? submit.attr('value') : $('input[name=PageSizeCache]').attr('value');
		pageSize = pageSize == 'All' ? 10000 : pageSize;
		var start = submit.attr('id').replace('PageNumber_', '') * pageSize;

		var data = {};

		// Add the filter data
		form.serializeArray().each(function(item) {
			data[item.name] = item.value;
		});

		data.start = start; // overwrite
		data.PageSize = pageSize; // overwrite

		$.ajax({
			type: form.attr('method'),
			url: form.attr('action'),
			data: $.param(data),
			beforeSend: function() {
				tab.addClass('loading');
			},
			complete: function() {
				tab.removeClass('loading');
			},
			success: function(data) {
				tab.html(data);
			}
		});

		e.preventDefault();
	};

	// Orders pagination submit handler
	$(document).on('submit', '#orders-pagination', paginationHandler);

	// Order refund radio buttons click handler : enable the amount input field
	// when refund type is Partial and disable it when refund type is Full
	$(document).on('click', '.refund-type', function(e) {
		var radio = $(e.target),
			value= radio.val(),
			td = radio.parents('td').first(),
			input = td.find('input[name=refund_amount]');
		if (value == 'partial') {
			input.attr('disabled', false);
		} else if (value == 'full') {
			input.attr('disabled', true);
		}
	});

	// Order refund form submit handler : implement it with ajax because 
	// it should refresh the content of only one table row
	$(document).on('submit', '.refund-form', function(e) {
		var form = $(e.target);
		var td = form.parents('td').first();
		var error = td.find('.payment-error');

		$.ajax({
			type: form.attr('method'),
			url: form.attr('action'),
			data: form.serialize(),
			beforeSend: function() {
				form.addClass('loading');
			},
			fail: function(data) {
				error.html(app.errorHTML.format(app.resources.unknown_error));
			},
			complete: function(data) {
				form.removeClass('loading');
				var data = $.parseJSON(data.responseText);
				if (data.code != '200') {
					error.html(app.errorHTML.format(data.error));
				} else {
					error.html(''); // clear any previous error message
					var refunds = data.refunds,
						refundsList = td.find('.payment-refunds-list'),
						refundsHTML = '',
						isFull = false;
					// display the refund
					for (var i = 0, length = refunds.length; i < length; i++) {
						var refund = $.parseJSON(refunds[i]);
						isFull = refund.type == 'full';
						// refund status row
						refundsHTML += '<div>' +
							app.resources.refund_status +
							' : ' +
							(isFull ? data.refundStatus : refund.status) +
							'</div>';
						// refund amount row
						refundsHTML += '<div>' +
							app.resources.refund_amount +
							' : ' +
							refund.amount +
							'</div>';
					}
					refundsList.html(refundsHTML);
					if (data.refundStatus == 'PENDING') {
						form.hide();
					}
					// if refund is full then remove the form
					if (isFull) {
						form.remove();
					// if refund is partial then remove the full refund option
					} else {
						var labels = form.find('label');
						if (labels.length > 1) {
							var label = form.find('label:first-child');
							label.remove();
						}
					}
				}
			}
		});

		e.preventDefault();
	});

	// Order transactions radio buttons click handler : enable the amount input field
	// when transaction type is partial and disable it when refund type is full
	$(document).on('click', '.transaction-type', function(e) {
		var radio = $(e.target),
			value= radio.val(),
			td = radio.parents('td').first(),
			input = td.find('input[name=transaction_amount]');
		if (value == 'partial') {
			input.attr('disabled', false);
		} else if (value == 'full') {
			input.attr('disabled', true);
		} else if (value == 'cancel') {
			input.attr('disabled', true);
		}
	});

	// Order transaction form submit handler : implement it with ajax because 
	// it should refresh the content of only one table row
	$(document).on('submit', '.transaction-form', function(e) {
		var form = $(e.target);
		var td = form.parents('td').first();
		var error = td.find('.payment-error');

		$.ajax({
			type: form.attr('method'),
			url: form.attr('action'),
			data: form.serialize(),
			beforeSend: function() {
				form.addClass('loading');
			},
			fail: function(data) {
				error.html(app.errorHTML.format(app.resources.unknown_error));
			},
			complete: function(data) {
				form.removeClass('loading');
				var data = $.parseJSON(data.responseText);
				if (data.code != '200') {
					error.html(app.errorHTML.format(data.error));
				} else {
					var transactions = data.transactions,
						transactionsList = td.find('.payment-transactions-list'),
						transactionsHTML = '',
						isFull = false;
					// display the transaction
					for (var i = 0, length = transactions.length; i < length; i++) {
						var transaction = $.parseJSON(transactions[i]);
						isFull = transaction.type == 'full';
						// transaction status row
						transactionsHTML += '<div>' +
							app.resources.transaction_status +
							' : ' +
							(isFull ? data.authorizationStatus : transaction.status) +
							'</div>';
						// transaction amount row
						if(transaction.type != "cancel"){
							transactionsHTML += '<div>' +
									app.resources.transaction_amount +
									' : ' +
									transaction.amount +
									'</div>';
						}
						
					}
					transactionsList.html(transactionsHTML);
					if (data.authorizationStatus == 'PENDING') {
						form.hide();
					}
					// if capture is partial then remove the full capture option
					if (!isFull) {
						var labels = form.find('label');
						if (labels.length > 1) {
							var label = form.find('label:first-child');
							label.remove();
						}
					}
				}
			}
		});

		e.preventDefault();
	});

	// Search tabs handler : display hidden tabs when proper tab buttons are clicked
	$(document).on('click', 'a.switch_link', function(e) {
		var a = $(e.target),
			tabId = a.attr('href').substring(a.attr('href').indexOf('#')+1),
			tabC = $('div#C'),
			tabD = $('div#D'),
			tabE = $('div#E');

		if (tabId == tabC.attr('id')) {
			tabC.show(); tabD.hide(); tabE.hide();
		} else if (tabId == tabD.attr('id')) {
			tabC.hide(); tabD.show(); tabE.hide();
		} else if (tabId == tabE.attr('id')) {
			tabC.hide(); tabD.hide(); tabE.show();
		}
	});

	// Search forms handler : submit using ajax
	$(document).on('submit', 'div#bm-payment-orders-search form', function(e) {
		var form = $(e.target);

		$.ajax({
			type: form.attr('method'),
			url: form.attr('action'),
			data: form.serialize(),
			beforeSend: function() {
				form.addClass('loading');
			},
			complete: function(data) {
				form.removeClass('loading');
				$('#bm-payment-orders-list').html(data.responseText);
			}
		});

		e.preventDefault();
	});

});
})(jQuery);