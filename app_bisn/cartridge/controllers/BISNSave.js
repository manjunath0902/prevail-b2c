'use strict';

/* API includes */
var Transaction = require('dw/system/Transaction');
var guard = require('app_adapter/cartridge/scripts/guard');
/*
 * If customer subscribe for back in stock notification , he will be notified once the product comes in in-stock.
 */
function bisn() {
    var args = {};
    var response = require('app_adapter/cartridge/scripts/util/Response');
    var bisnResult;
    args.Pid = request.httpParameterMap.bisnpid.stringValue;
    args.Email = request.httpParameterMap.bisnemail.stringValue;

    Transaction.wrap(function() {
        bisnResult = require('~/cartridge/scripts/cart/Bisn').bisnRequest(args);
    });
    if (bisnResult) {
        response.renderJSON({
            success: true
        });
    } else {
        response.renderJSON({
            success: false
        });
    }
    return;
}
/*
 * subscription for back in stock notification
 */
exports.Bisn = guard.ensure(['get', 'http'], bisn);