'use strict';

/**
 * Controller that renders pages required for image comparison.
 *
 * @module controllers/SessionBased
 */

/* Script Modules */
var app = require('app_adapter/cartridge/scripts/app');
var guard = require('app_adapter/cartridge/scripts/guard');


/*API Includes*/
var OrderMgr = require('dw/order/OrderMgr');
var StoreMgr = require('dw/catalog/StoreMgr');
var SystemObjectMgr = require('dw/object/SystemObjectMgr');


function reviewSummary() {
    require('bc_QATools/cartridge/scripts/sessionBased').ReviewSummary();
}

function loadOrder() {
    require('bc_QATools/cartridge/scripts/sessionBased').LoadOrder();
}

function register() {
    require('bc_QATools/cartridge/scripts/sessionBased').Register();
}

function showConfirmation() {
    var orderNo = request.httpParameterMap.orderNo.stringValue;
    var order = OrderMgr.getOrder(orderNo);
    app.getController('COSummary').ShowConfirmation(order);
}

function findStore() {

    session.forms.storelocator.countryCode.value = request.httpParameterMap.countryCode.value;
    session.forms.storelocator.country.value = request.httpParameterMap.country.value;
    session.forms.storelocator.postalCode.value = request.httpParameterMap.postalCode.value;
    session.forms.storelocator.state.value = request.httpParameterMap.state.value;
    session.forms.storelocator.distanceUnit.value = 'mi';
    session.forms.storelocator.maxdistance.value = request.httpParameterMap.maxdistance.intValue;

    var searchResult = null;
    var formgroup = session.forms.storelocator;
    var searchKey = null;
    var stores = null;
    if (session.forms.storelocator.country.value) { //findbycountry
        searchKey = formgroup.country.htmlValue;
        stores = SystemObjectMgr.querySystemObjects('Store', 'countryCode = {0}', 'countryCode desc', searchKey);
        if (empty(stores)) {
            searchResult = null;
        } else {
            searchResult = {
                'stores': stores,
                'searchKey': searchKey,
                'type': 'findbycountry'
            };
        }

    } else if (session.forms.storelocator.state.value) { //findbystate
        searchKey = formgroup.state.htmlValue;
        stores = null;
        if (!empty(searchKey)) {
            stores = SystemObjectMgr.querySystemObjects('Store', 'stateCode = {0}', 'stateCode desc', searchKey);
        }

        if (empty(stores)) {
            searchResult = null;
        } else {
            searchResult = {
                'stores': stores,
                'searchKey': searchKey,
                'type': 'findbystate'
            };
        }
    } else if (session.forms.storelocator.postalCode.value) { //findbyzip
        searchKey = formgroup.postalCode.value;
        var storesMgrResult = StoreMgr.searchStoresByPostalCode('US', searchKey, formgroup.distanceUnit.value, formgroup.maxdistance.value);
        stores = storesMgrResult.keySet();
        if (empty(stores)) {
            searchResult = null;
        } else {
            searchResult = {
                'stores': stores,
                'searchKey': searchKey,
                'type': 'findbyzip'
            };
        }
    }

    if (searchResult) {
        /*****************************PREVAIL - Store Locator Integration*****************************************/
        if (dw.system.Site.getCurrent().getCustomPreferenceValue('storeLocatorService').toString() === 'PREVAIL') {
            app.getView({
                StoresCount: (searchResult.stores instanceof dw.util.SeekableIterator) ? searchResult.stores.count : searchResult.stores.size(),
                Stores: new dw.util.ArrayList(searchResult.stores),
                SearchString: searchResult.searchKey
            }).render('storelocator/storelocatorresultswithmap');
        } else {
            app.getView('StoreLocator', searchResult).render('storelocator/storelocatorresults');
        }
    } else {
        app.getView('StoreLocator')
            .render('storelocator/storelocator');
    }
}

/*
 * Exposed web methods
 */

/** Renders summary page
 * @see module:controllers/SessionBased~reviewSummary */
exports.ReviewSummary = guard.ensure(['get'], reviewSummary);
/** loads basket from an order and redirects to a page based on location provided. 
 * @see module:controllers/SessionBased~loadOrder */
exports.LoadOrder = guard.ensure(['get'], loadOrder);
/** Authenticates the customer and loads the order.
 * @see module:controllers/SessionBased~register */
exports.Register = guard.ensure(['get'], register);
/** renders order confirmation page.
 * @see module:controllers/SessionBased~showConfirmation */
exports.ShowConfirmation = guard.ensure(['get'], showConfirmation);
/** renders store locators results page based on the parameters provided. 
 * @see module:controllers/SessionBased~findStore */
exports.FindStore = guard.ensure(['get'], findStore);
