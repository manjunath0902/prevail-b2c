'use strict';

/**
 * This pipeline is called by the CSS tool and gets a sessionID passed to it as a parameter. 
 * This parameter is then used in REST service calls to PFS to return session information in a JSON format.
 * Additional Parameters can be added to the case statement inside of the CSSSessionDataRetrieval.ds script to handle additional functions.
 * The CSSLOGIN should always be processed as it will have the Agent login information and the customer profile to login to.
 * The list of fields to request in the REST call are populated in the CSSDataKeys site preference
 * 
 * @module controllers/CSSSession
 */

/*API Includes*/
var Transaction = require('dw/system/Transaction');
var AgentUserMgr = require('dw/customer/AgentUserMgr');
var CustomerMgr = require('dw/customer/CustomerMgr');

/* Script Modules */
var app = require('app_adapter/cartridge/scripts/app');
var guard = require('app_adapter/cartridge/scripts/guard');

/**
 * Starting point for iCA session. This function will be called by iCA with one parameter SessionID. 
 * The same SessionID will be posted to loginAgent function. 
 */
function sessionStart() {

    var sessionID = request.httpParameterMap.SessionID.stringValue;
    Transaction.wrap(function() {
        session.custom.iCASessionID = sessionID;
    })
    app.getView({
        SessionID: sessionID
    }).render('utility/post');
}

/**
 * Logs-in the AgentUser
 */
function loginAgent() {

    AgentUserMgr.logoutAgentUser();

    var args = {
        'SessionID': request.httpParameterMap.SessionID.stringValue
    };
    var cssSessionData = require('~/cartridge/scripts/session/CSSSessionDataRetrieval');
    var cssSessionDataResult = cssSessionData.cssSessionDataRetrieval(args);

    if (cssSessionDataResult === PIPELET_ERROR) {
        app.getView({
            ErrorMessage: args.ErrorMessage
        }).render('error/failedcsssession');
        return;
    } else {
        Transaction.wrap(function() {
            var loginAgentUserResult = AgentUserMgr.loginAgentUser(args.CSSUser, args.CSSPword);
            if (loginAgentUserResult.error) {
                app.getView({
                    Status: loginAgentUserResult,
                    ErrorMessage: args.ErrorMessage
                }).render('error/failedcssagentlogin');
            } else {
                app.getView({
                    NextPipeline: 'CSSSession-Redirect',
                    SessionID: request.httpParameterMap.SessionID.stringValue
                }).render('utility/postcustomer');
            }
        })
    }
}

/**
 * Redirecting to CSSSession-LoginCustomer. This is required to bind customer to current session.
 */
function redirect() {
    app.getView({
        SessionID: request.httpParameterMap.SessionID.stringValue,
        NextPipeline: 'CSSSession-LoginCustomer'
    }).render('utility/postcustomer');
}

/**
 * Gets CSR login /password and a customer number for use in logging in on behalf of customer.
 */
function loginCustomer() {
    var args = {
        'SessionID': request.httpParameterMap.SessionID.stringValue
    };
    var cssSessionData = require('~/cartridge/scripts/session/CSSSessionDataRetrieval');
    var cssSessionDataResult = cssSessionData.cssSessionDataRetrieval(args);

    if (cssSessionDataResult === PIPELET_ERROR) {
        app.getView({
            ErrorMessage: args.ErrorMessage
        }).render('error/failedcsssession');
        return;
    } else {
        var getCustomerProfile = require('~/cartridge/scripts/session/GetCustomerProfile');
        var getCustomerProfileResult = getCustomerProfile.getCustomerProfile(args);
        var customerProfile = args.customerProfile;
        
        if (getCustomerProfileResult !== PIPELET_ERROR) {
            if (customerProfile != null && customerProfile.credentials != null) {
                var customer = CustomerMgr.getCustomerByLogin(customerProfile.credentials.login);
                Transaction.wrap(function() {
                    return AgentUserMgr.loginOnBehalfOfCustomer(customer);
                });
            }
        }
      
        if (args.SourceCode != '') {
            session.setSourceCode(args.SourceCode);
        }

        if (args.LanguageCode != '') {
            request.setLocale(args.LanguageCode);
        }

        session.custom.CSSPromotionCode = args.PromotionCode ? cssSessionDataResult.PromotionCode : '';
        session.custom.CSSRelatedOrder = args.RelatedOrder;
        session.custom.CSSReturnExchangeDiscount = args.ReturnExchangeDiscount;
        session.custom.CSSWarrantyProducts = args.WarrantyProducts;
        session.custom.CSSSessionActive = true;
        session.custom.CSSOrderType = args.OrderType;
        session.custom.CSSSubscriptionReasonCode = args.ReasonCode;
        session.custom.CSSStoreNumber = args.StoreNumber;
        session.custom.CSSUserId = args.CSSUser;

        if (args.RequestedPageQueryString.length > 0) {
            app.getView({
                Location: dw.web.URLUtils.https(args.RequestedPage) + args.RequestedPageQueryString
            }).render('utility/cssredirect');
        } else {
            app.getView({
                Location: dw.web.URLUtils.https(args.RequestedPage)
            }).render('utility/cssredirect');
        }
    }
}

/**
 * Once session ends, logs-out the AgentUser.
 */
function sessionEnd() {
    AgentUserMgr.logoutAgentUser();
    app.getView().render('common/http_200');
}

/*Module Exports*/
/** Starts the session and gets the sessionID from the current session.
 * @see {@link module:controllers/CSSSession~sessionStart} */
exports.SessionStart = guard.ensure(['https'], sessionStart);
/** Logs in the AgentUser and retrieves CSSSessionData
 * @see {@link module:controllers/CSSSession~loginAgent} */
exports.LoginAgent = guard.ensure(['https', 'post'], loginAgent);
/** Renders loginCustomer
 * @see {@link module:controllers/CSSSession~redirect} */
exports.Redirect = guard.ensure(['https', 'post'], redirect);
/** AgentUser logs in on behalf of the customer. 
 * @see {@link module:controllers/CSSSession~loginCustomer} */
exports.LoginCustomer = guard.ensure(['https', 'post'], loginCustomer);
/** Ends the current session.
 * @see {@link module:controllers/CSSSession~sessionEnd} */
exports.SessionEnd = guard.ensure(['get'], sessionEnd);
