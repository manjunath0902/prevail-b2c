/*******************************************************************
 * 
 *  Controls the view state of the registration module
 * 
 ******************************************************************/

if(typeof(Demandware)=='undefined')Demandware = {};
if(!Demandware.Gigya)Demandware.Gigya = {};

Demandware.Gigya.Registration = new function(){
	var _divParams = null;
	var _divLinkAccount = null;
	var _isActive = null;
	var _isGigyaAuthenticated = null;
	var _isGigyaloggedIn = null;
	var _accountExists = null;
	var _frmLinkAccount = null;
	var _btnLinkAccount = null;
	var _txtUserName = null;
	var _txtPassword = null;
	var _profileEmail = null;
	var _twitterRegistration = null;
	
	var _frmRegister = null;
	var _gigyaFirstName = null;
	var _gigyaLastName = null;
	var _gigyaEmail = null;
		
	new function(){
		$(document).bind("ready", initialize);
		$(window).bind("unload", dispose);
	}
	
	function initialize(){
		initializeElements();
				
		//regular registration 
		if(!_isActive || !_isGigyaAuthenticated){
			if(!_profileEmail) //if editing existing DW account 
				setGigyaRegisterAction(true);
			_frmRegister.css('visibility','visible');
			_divLinkAccount.hide();
			return;
		}
		
		if(_isGigyaloggedIn){ //editing profile page
			_divLinkAccount.hide();
			_frmRegister.css('visibility','visible');
		}
		else{ //registering with Gigya
			prepareGigyaFormFields();
			_btnLinkAccount.bind('click', btnLinkAccount_OnClick);
			setGigyaRegisterAction();
		}
		hidePasswordFields();
	}
	
	function initializeElements(){
		_divParams = $('#divGigyaRegistrationParams');
		_isActive = _divParams.find('#txtGigyaActive').val()=='true';
		_isGigyaAuthenticated = _divParams.find('#txtGigyaAuthenticated').val()=='true';
		_isGigyaloggedIn = _divParams.find('#txtGigyaLoggedIn').val()=='true';
		_accountExists = _divParams.find('#txtGigyaAccountExists').val()=='true';
		_profileEmail = _divParams.find('#txtGigyaRegistrationCurrentAccount').val();
		_twitterRegistration = _divParams.find('#txtGigyaRegistrationTwitterReg').val()=='true';
		
		_divLinkAccount = $("#divGigyaLinkAccount");
		_txtUserName = _divLinkAccount.find('#dwfrm_gigyalink_username');
		_txtPassword = _divLinkAccount.find('#dwfrm_gigyalink_password');
		_frmLinkAccount = _divLinkAccount.find("#frmLinkAccount");
		_btnLinkAccount = _divLinkAccount.find("div.formactions button[name='dwfrm_gigyalink_linkaccount']");
		
		var formId = _divParams.find("#txtGigyaRegistrationFormId").val()||"RegistrationForm";
		_frmRegister = $("#"+formId);
		_gigyaFirstName = _divParams.find("#txtGigyaRegistrationFirstNameValue").val();
		_gigyaLastName = _divParams.find("#txtGigyaRegistrationLastNameValue").val();
		_gigyaEmail = _divParams.find("#txtGigyaRegistrationEmailValue").val();
	}
	
	function setGigyaRegisterAction(dwRegistration){
		var registerAction = '';
		
		if(!dwRegistration && _twitterRegistration)
			registerAction = _divParams.find('#txtGigyaRegistrationTwitterAction').val();
		else
			registerAction = _divParams.find('#txtGigyaRegistrationRegisterAction').val();
		if(registerAction)
			_frmRegister.attr('action',registerAction);
	}
	
	function prepareGigyaFormFields(){
		clearFormFields();
		_frmRegister.find('#dwfrm_profile_customer_firstname').val(_gigyaFirstName);
		_frmRegister.find('#dwfrm_profile_customer_lastname').val(_gigyaLastName);
		
		if(_profileEmail) //authenticated with DW but not Gigya
		{
			_txtUserName.val(_profileEmail);
			_txtUserName.addClass('readonly');
			_txtUserName.attr('readonly','readonly');
			_divLinkAccount.find('#divLinkCurrentAccount').removeClass('remove');
		}
		else
		{
			if(_twitterRegistration)
			{
				prepareTwitterFields();
			}
			else //facebook-like log-in
			{
				prepareFacebookFields();
			}	
		}
	}
	
	function prepareTwitterFields()
	{
		_divLinkAccount.addClass('remove');
		_frmRegister.css('visibility','visible');
		
	}
	
	function prepareFacebookFields()
	{
		_frmRegister.css('visibility','visible');
		if(_accountExists){
			_divLinkAccount.find('#divAccountFound').removeClass('remove');
			_txtUserName.val(_gigyaEmail);
		}
		else{
			_divLinkAccount.find('#divInstructions').removeClass('remove');
			_frmRegister.find('#dwfrm_profile_customer_email').val(_gigyaEmail);
			_frmRegister.find('#dwfrm_profile_customer_emailconfirm').val(_gigyaEmail);
		}
	}
	
	function clearFormFields(){
		_txtUserName.val('');
		_txtPassword.val('');
	}
	
	function random(length) {
	    var validChars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');
	    var randomValue = '';
	    for (var i = 0; i < length; i++) {
	    	var randomIndex = Math.floor(Math.random() * validChars.length);
	    	randomValue += validChars[randomIndex];
	    }
	    return randomValue;
	}
	
	function hidePasswordFields(){
		var temp = random(10);
		var field = _frmRegister.find('#dwfrm_profile_login_password');
		field.closest('div.form-row').hide();
		field.val(temp);
		
		field = _frmRegister.find('#dwfrm_profile_login_passwordconfirm');
		field.closest('div.form-row').hide();
		field.val(temp);
	}
		
	
	function dispose(){
		_divParamsContainer = null;
		_btnLinkAccount.unbind('click', btnLinkAccount_OnClick)
	}
	
	function btnLinkAccount_OnClick(){
		var username = _txtUserName.val();
		var password = _txtPassword.val();
		
		if(username && password)
			_frmLinkAccount.submit();
		else
			return false;
	}
}
