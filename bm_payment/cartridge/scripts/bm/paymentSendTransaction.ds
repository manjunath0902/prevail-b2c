/**
* Send transactions
*
* @input HttpParameterMap : Object
* @output Response : Object
*
*/
importPackage( dw.system );

importScript("int_payment:lib/libPayment.ds");

function execute( args : PipelineDictionary ) : Number {
	var log : Logger = Logger.getLogger("payment");
	var params = args.HttpParameterMap;
	args.Response = {'code': '200', 'error': ''};

	var orderNo = params['transaction_orderid'].value;
	var transactionType = params['transaction_type'].value;
	var amount = params['transaction_amount'].value;

	if (empty(orderNo)) {
		log.error('paymentSendTransaction.ds: Missing required input parameter transaction_orderid.');
		return PIPELET_ERROR;
	}

	if (transactionType == 'partial-capture' && empty(amount)) {
		args.Response.code = '500';
		args.Response.error = dw.web.Resource.msg('forms.payment.orders.error.amount.missing', 'forms', null);
		return PIPELET_ERROR;
	}

	var order = null;
	var paymentMgr = null;
	var resultObj = null;
	var status = '';
	var operation = '';
	var paymentMethodID = null;
	var locale = null;
	var today = new Date();
	amount = empty(amount) ? amount : new Number(amount); // String => Number

	try {
		order = dw.order.OrderMgr.getOrder(orderNo);
		orderTotal = order.totalGrossPrice.getValue(); // Money => Number

		// Order status validation
		if (order.status == dw.order.Order.ORDER_STATUS_FAILED || 
			order.status == dw.order.Order.ORDER_STATUS_CREATED) {
			args.Response.code = '500';
			args.Response.error = dw.web.Resource.msg('forms.payment.orders.error.status.not_allowed', 'forms', null);
			return PIPELET_ERROR;
		}

		if (!empty(order.custom.payment_data)) {
			var data : Object = JSON.parse(order.custom.payment_data);
			paymentMethodID = data.paymentMethodID;
			locale = data.locale;
		}

		if (transactionType == 'full') {
			operation = 'SAL';
			amount = orderTotal;
		} else if (transactionType == 'partial') {
			operation = 'SAL';
		} else {
			operation = 'DES';
		}
		//create object for request
		paymentObj = new PaymentCtnr(order, order.orderNo, amount, paymentMethodID, null, locale);
		
		//save result from request
		resultObj = paymentObj.sendDirectLinkMaintenanceRequest(operation, null, null);

		if (!empty(resultObj)) {
			status = resultObj.hasOwnProperty("status") ? resultObj.status : '';
		}

		var paymentInstrument : OrderPaymentInstrument = order.getPaymentInstruments()[0];
		var paymentTransaction = paymentInstrument.paymentTransaction;
		paymentTransaction.custom.payment_paymentID = paymentInstrument.paymentTransaction.transactionID;
		
		//update order
		if (status == '9') {
			order.custom.payment_needCapture = false;
			order.custom.payment_captureAttempts += 1;
			order.custom.payment_authorizationStatus = 'REQUESTED';
			paymentTransaction.custom.paymentStatus=status;
			paymentTransaction.custom.paymentStatusDescription = getPaymentStatusDescription(paymentTransaction.custom.paymentStatus);
			order.setPaymentStatus(dw.order.Order.PAYMENT_STATUS_PAID);
		} else if (status == '91') {
			order.custom.payment_authorizationStatus = 'PENDING';
			order.custom.payment_captureAttempts += 1;
			paymentTransaction.custom.paymentStatus=status;
			paymentTransaction.custom.paymentStatusDescription = getPaymentStatusDescription(paymentTransaction.custom.paymentStatus);
		} else if (status == '92') {
			order.custom.payment_authorizationStatus = 'UNCERTAIN';
			order.custom.payment_captureAttempts += 1;
			paymentTransaction.custom.paymentStatus=status;
			paymentTransaction.custom.paymentStatusDescription = getPaymentStatusDescription(paymentTransaction.custom.paymentStatus);
		} else if (status == '93') {
			order.custom.payment_authorizationStatus = 'REFUSED';
			order.custom.payment_captureAttempts += 1;
			paymentTransaction.custom.paymentStatus=status;
			paymentTransaction.custom.paymentStatusDescription = getPaymentStatusDescription(paymentTransaction.custom.paymentStatus);
		} else if (status == '94') {
			order.custom.payment_authorizationStatus = 'DECLINED_BY_ACQUIRER';
			order.custom.payment_captureAttempts += 1;
			paymentTransaction.custom.paymentStatus=status;
			paymentTransaction.custom.paymentStatusDescription = getPaymentStatusDescription(paymentTransaction.custom.paymentStatus);
		} else if (status == '95') {
			order.custom.payment_authorizationStatus = 'HANDLED_BY_MERCHANT';
			order.custom.payment_captureAttempts += 1;
			paymentTransaction.custom.paymentStatus=status;
			paymentTransaction.custom.paymentStatusDescription = getPaymentStatusDescription(paymentTransaction.custom.paymentStatus);
		} else if (status == '6') {
			order.custom.payment_authorizationStatus = 'CANCELLED';
			paymentTransaction.custom.paymentStatus=status;
			paymentTransaction.custom.paymentStatusDescription = getPaymentStatusDescription(paymentTransaction.custom.paymentStatus);
		} else if (status == '61') {
			order.custom.payment_authorizationStatus = 'PENDING';
			paymentTransaction.custom.paymentStatus=status;
			paymentTransaction.custom.paymentStatusDescription = getPaymentStatusDescription(paymentTransaction.custom.paymentStatus);
		} else if (status == '62') {
			order.custom.payment_authorizationStatus = 'UNCERTAIN';
			paymentTransaction.custom.paymentStatus=status;
			paymentTransaction.custom.paymentStatusDescription = getPaymentStatusDescription(paymentTransaction.custom.paymentStatus);
		} else if (status == '63') {
			order.custom.payment_authorizationStatus = 'REFUSED';
			paymentTransaction.custom.paymentStatus=status;
			paymentTransaction.custom.paymentStatusDescription = getPaymentStatusDescription(paymentTransaction.custom.paymentStatus);
		}

		if (status == '0') {
			args.Response.code = '500';
			args.Response.error = 'Request invalid or incomplete';
			log.error("paymentSendTransaction.ds: Direct Link Maintenance Request invalid or incomplete; OrderNo: {0}", order.orderNo);
		} else {
			order.custom.payment_payidSUB += 1 ;
			args.Response.authorizationStatus = order.custom.payment_authorizationStatus;

			var transactions : Array = order.custom.payment_transactions.slice();
			transactions.push(JSON.stringify({
				amount: amount,
				date: today,
				type: transactionType,
				status: order.custom.payment_authorizationStatus
			}));
			order.custom.payment_transactions = transactions;
			// format the amount
			var transactions2 = transactions.map(function(item) {
				var obj = JSON.parse(item);
				if(operation == 'DES'){
					var amount = "";
				}else{
					var amount = new dw.value.Money(obj.amount, order.currencyCode).toFormattedString();
				}
				return JSON.stringify({
					amount: amount,
					date: obj.date,
					type: obj.type,
					status: obj.status
				});
			});
			args.Response.transactions = transactions2;
		}
	} catch (error) {
		args.Response.code = '500';
		if (error.paymentErrorMsgPlus) {
			args.Response.error = error.paymentErrorMsgPlus;
		} else {
			args.Response.error = error.toString();
		}
		log.error("paymentSendTransaction.ds: {0}", error);
		return PIPELET_ERROR;
	}

	return PIPELET_NEXT;
}
