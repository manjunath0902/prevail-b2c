'use strict';

/**
 * Validates shipping address and returns true if address is valid /partially valid, false otherwise.
 * 
 * @param {Object} args
 * @returns {Boolean}
 */
function validateShippingAddress(args) {
    if (request.httpParameterMap.bypassDAV && request.httpParameterMap.bypassDAV.value == 'false') {
        var groupOne = require('int_groupone/cartridge/scripts/services/GroupOne');
        args.AddressForm = session.forms.singleshipping.shippingAddress.addressFields;
        var validateShippingAddressResult = groupOne.validateAddress(args);

        if (validateShippingAddressResult === PIPELET_ERROR) {
            return false;
        } else {
            //Nullify variables
            args.ErrorDescription = null;
            args.ErrorFlag = null;
            args.RequestAddress = null;
            return true;
        }
    } else {
        //Nullify variables
        args.ErrorDescription = null;
        args.ErrorFlag = null;
        args.RequestAddress = null;
        return true;
    }
}

/**
 * Validates shipping address and returns true if address is valid /partially valid, false otherwise.
 * 
 * @param {Object} args
 * @returns {Boolean}
 */
function validateBillingAddress(args) {
    if (request.httpParameterMap.bypassDAV && request.httpParameterMap.bypassDAV.value == 'false') {
        var groupOne = require('int_groupone/cartridge/scripts/services/GroupOne');
        args.AddressForm = session.forms.billing.billingAddress.addressFields;
        var validateBillingAddressResult = groupOne.validateAddress(args);

        if (validateBillingAddressResult === PIPELET_ERROR) {
            return false;
        } else {
            //Nullify variables
            args.ErrorDescription = null;
            args.ErrorFlag = null;
            args.RequestAddress = null;
            return true;
        }
    } else {
        //Nullify variables
        args.ErrorDescription = null;
        args.ErrorFlag = null;
        args.RequestAddress = null;
        return true;
    }
}

/*Module Exports*/
exports.ValidateShippingAddress = validateShippingAddress;
exports.ValidateBillingAddress = validateBillingAddress;