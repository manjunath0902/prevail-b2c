// Copyright � 2013 Priority Fulfillment Services, Inc. All rights reserved.
/**
 * Creates the text of the mail subject.
 *
 * @input   joblogname  : String
 * @input   hasErrors   : Boolean
 * @input   hasWarns    : Boolean
 * @output  MailSubject : String
 *
 */

importPackage( dw.system );

function execute( pdict : PipelineDictionary ) : Number {

  var job   : String = pdict.joblogname;
  var subj  : String = "Your Job (" + job + ") on instance ";

  switch(System.getInstanceType()) {
    case System.DEVELOPMENT_SYSTEM:
      subj += "DEVELOPMENT";
      break;
    case System.PRODUCTION_SYSTEM:
      subj += "PRODUCTION";
      break;
    case System.STAGING_SYSTEM:
      subj += "STAGING";
      break;
    default:
      subj += "UNKNOWN";
      break;
  }

  if (pdict.hasErrors && pdict.hasWarns) {
    pdict.MailSubject = "ERRORS/WARNINGS " + subj + " has produced ERRORS and WARNINGS";
  } else if (pdict.hasErrors) {
    pdict.MailSubject = "ERRORS " + subj + " has produced ERRORS";
  } else if (pdict.hasWarns) {
    pdict.MailSubject = "WARNINGS " + subj + " has produced WARNINGS";
  } else {
    pdict.MailSubject = "ERRORS/WARNINGS " + subj + " has produced ERRORS/WARNINGS";
  }
  return PIPELET_NEXT;
}
