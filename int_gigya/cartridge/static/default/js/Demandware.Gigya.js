/*******************************************************************
 * 
 *  Contains global setup and common utility script methods
 * 
 ******************************************************************/

if(typeof(Demandware)=='undefined')Demandware = {};

Demandware.Gigya = new function(){
	var _this = this;
	var _loginCallbackUrl = null;
	var _logoutUrl = null;
	
	_this.Version = null;
	_this.PrivacyLevel = null;
	_this.publishAction = publishAction;
	_this.logout = logout;
	
	
	//constructor
	new function(){
		$(document).bind("ready", initialize);
		$(window).bind("unload", dispose);
	}
	
	function initialize(){
		_this.Version = $('#txtGigyaVersion').val();
		_this.PrivacyLevel = $('#txtGigyaPrivacyLevel').val()||'private';
		_loginCallbackUrl = $('#txtGigyaLoginSuccessUrl').val();
		_logoutUrl = $('#txtGigyaLogoutUrl').val();
		_sitelogoutUrl = $('#txtSiteLogoutUrl').val();
		_userManagementSettings = $('#txtGigyaUserManagementSettings').val();
		
		bindLogoutEvents();
		setupAccountHandler();
		setupGameNotification();
	}
	
	function publishAction(response, feedId){
		var publishParams = {
				scope:'internal'
				,privacy:_this.PrivacyLevel
				,userAction:response.context
			};
		if(feedId)
			publishParams.feedID = feedId;
		
		gigya.services.socialize.publishUserAction(publishParams); 	
	}
	
	function logout(evt){
		var url = evt.target.getAttribute('href');
		gigya.socialize.logout({callback:logoutCallback});
		$.ajax({type: 'GET',url: _logoutUrl});
		
		if(url)
			return false;
		
		function logoutCallback(data)
		{
			if(url)
				window.location.href = url;
		}
	}
	
	function setupGameNotification(){
		if($('#txtGigyaGameNotification').val()=='true'){
			gigya.socialize.getUserInfo({callback:validateUser});
		}
	}
	
	function setupAccountHandler(){
		if (_userManagementSettings === "2") {
			gigya.accounts.addEventHandlers({
					onLogin:onLogin_Complete,
					onLogout:onLogout_Complete
				}
			);
			
		} else {
			gigya.socialize.addEventHandlers({
					onLogin:onLogin_Complete,
					onLogout:onLogout_Complete
				} 
			);
		}
	}
	
	function onLogin_Complete(evt){
		if(evt.context &&  evt.context.source && evt.context.source=='loginPlugin')
			return;
		
		var loginUrl = _loginCallbackUrl +
					'?isSiteUID=true' +
					'&UID='+encodeURIComponent(evt.UID) +
					'&UIDSignature='+encodeURIComponent(evt.UIDSignature) +
					'&signatureTimestamp='+encodeURIComponent(evt.signatureTimestamp)
		
		window.location.href=loginUrl;		
	}
	
	function onLogout_Complete(){
		self.location.assign(_sitelogoutUrl);
	}
	
	function validateUser(response){
		if(response.user && response.user.isLoggedIn)
			gigya.gm.showNotifications({});
	}
	
	function bindLogoutEvents(){
		$('ul.menu-utility-user li a[href$="Logout"]').bind('click',logout);
		$('div.pt_account span.account-logout a').bind('click',logout);
	}
	
	function dispose(){
		_this = null;
	}
}