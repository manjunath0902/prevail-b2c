'use strict';
/**
 * Controller that stores customer email preferences in custom object for email subscription.
 *
 * @module controllers/ExactTargetEmailSignUp
 */

/* API includes */
var URLUtils = require('dw/web/URLUtils');
var Transaction = require('dw/system/Transaction');

/* Script Modules */
var app = require('app_adapter/cartridge/scripts/app');
var guard = require('app_adapter/cartridge/scripts/guard');

function footer() {
    app.getView({
        ContinueURL: URLUtils.https('ExactTargetEmailSignUp-FooterSignUp')
    }).render('emailsignup/emailsubscription');
}

function footerSignUp() {
    app.getForm('emailsubscribe').handleAction({
        error: function() {
            app.getView({
                ContinueURL: URLUtils.https('ExactTargetEmailSignUp-FooterSignUp')
            }).render('emailsignup/emailsubscription');
        },

        confirm: function() {

            var footerSignUp = app.getForm('emailsubscribe').object;
            var args = {}, result;
            args.BirthDate = footerSignUp.birthdate.day.value;
            args.BirthMonth = footerSignUp.birthdate.month.value;
            args.BirthYear = footerSignUp.birthdate.year.value;
            args.City = footerSignUp.city.value;
            args.Country = footerSignUp.city.value;
            args.Electronics = footerSignUp.interests.electronics.value;
            args.Email = footerSignUp.email.value;
            args.EmailSource = footerSignUp.emailSource.value;
            args.FirstName = footerSignUp.firstName.value;
            args.Gender = footerSignUp.gender.value;
            args.Kids = footerSignUp.interests.kids.value;
            args.LastName = footerSignUp.lastName.value;
            args.Mens = footerSignUp.interests.mens.value;
            args.NewProducts = footerSignUp.preferences.newProducts.value;
            args.Promotions = footerSignUp.preferences.promotions.value;
            args.State = footerSignUp.states.state.value;
            args.Womens = footerSignUp.interests.womens.value;

            Transaction.wrap(function() {
                var EmailSignup = require('~/cartridge/scripts/util/EmailSignup.ds');
                result = EmailSignup.createObject(args);
            });

            if (result === 0) {
                app.getView({
                    ContinueURL: URLUtils.https('ExactTargetEmailSignUp-FooterSignUp')
                }).render('emailsignup/emailsubscription');
                return;
            }

            //Clear form.
            app.getForm('emailsubscribe').clear();

            app.getView().render('emailsignup/emailsingupsuccess');
        }
    });
}

function billing(options) {
    Transaction.wrap(function() {
        var billingSignUp = require('~/cartridge/scripts/util/EmailSignup.ds');
        billingSignUp.createObject(options);
    });
    return true;
}


function myAccount(options) {
    Transaction.wrap(function() {
        var accountSignUp = require('~/cartridge/scripts/util/EmailSignup.ds');
        accountSignUp.createObject(options);
    });
    return true;
}


/*Module Exports*/

/*
 * Local methods
 */
exports.billing = billing;
exports.myAccount = myAccount;

/* Web exposed methods */
/** Renders the Footer SignUp Page.
 * @see {@link module:int_exacttarget/cartridge/controllers/ExactTargetEmailSignUp~Footer} */
exports.Footer = guard.ensure(['https'], footer);

/** Action for Footer SignUp Page.
 * @see {@link module:int_exacttarget/cartridge/controllers/ExactTargetEmailSignUp~footerSignUp} */
exports.FooterSignUp = guard.ensure(['post', 'https'], footerSignUp);
