'use strict';

/* API Includes */
var Cart = require('~/cartridge/scripts/models/CartModel');
var PaymentMgr = require('dw/order/PaymentMgr');
var Transaction = require('dw/system/Transaction');

/* Script Modules */
var app = require('~/cartridge/scripts/app');

/**
 * Verifies a credit card against a valid card number and expiration date and possibly invalidates invalid form fields.
 * If the verification was successful a credit card payment instrument is created.
 */
function Handle(args) {
    var cart = Cart.get(args.Basket);
    if (!session.custom.hostedTokenizationSucces) {
        return {
            error: true
        };
    }
    Transaction.wrap(function() {
        var pdict = dw.system.Pipeline.execute('PAYMENT_HOSTED_TOKENIZATION_PAGE_CREDIT_CARD-Handle', {
            Basket: cart.object
        });
        if (pdict.EndNodeName == 'error') {
            return {
                error: true
            };
        }
    });

    return {
        success: true
    };
}

/**
 * Authorizes a payment using a credit card. The payment is authorized by using the BASIC_CREDIT processor
 * only and setting the order no as the transaction ID. Customizations may use other processors and custom
 * logic to authorize credit card payment.
 */
function Authorize(args) {
    var orderNo = args.OrderNo;
    var paymentInstrument = args.PaymentInstrument;
    var paymentProcessor = PaymentMgr.getPaymentMethod(paymentInstrument.getPaymentMethod()).getPaymentProcessor();

    Transaction.wrap(function() {
        paymentInstrument.paymentTransaction.transactionID = orderNo;
        paymentInstrument.paymentTransaction.paymentProcessor = paymentProcessor;
    });

    var pdict = dw.system.Pipeline.execute('PAYMENT_HOSTED_TOKENIZATION_PAGE_CREDIT_CARD-Authorize', {
        Order: args.Order,
        CurrentCustomer: customer,
        CurrentRequest: request,
        OrderNo: args.OrderNo,
        PaymentMethodID: paymentInstrument.getPaymentMethod()
    });

    if (pdict.EndNodeName === 'authorized') {
        return {
            success: true
        };
    } else if (pdict.EndNodeName === 'error') {
        return {
            error: true
        };
    } else if (pdict.Template && pdict.EndNodeName === 'redirect') {
        return {
            template: pdict.Template,
            params:{
            	PaymentRequestParameters: pdict.PaymentRequestParameters,
            	HTML: pdict.HTML,
            },
            isRedirect: true
        };
    } else if (pdict.RedirectLocation && pdict.EndNodeName === 'redirect') {
        return {
            redirectUrl: pdict.RedirectLocation,
            params:{
            	PaymentRequestParameters: pdict.PaymentRequestParameters
            },
            isRedirect: true
        };
    }
}

/*
 * Module exports
 */

/*
 * Local methods
 */
exports.Handle = Handle;
exports.Authorize = Authorize;
