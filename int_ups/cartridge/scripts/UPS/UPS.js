'use strict';

/**
 * Validates shipping address and returns true if address is valid, false otherwise.
 * 
 * @param {Object} args
 * @returns {Boolean}
 */
function validateAddress(args) {
    if (request.httpParameterMap.bypassDAV && request.httpParameterMap.bypassDAV.value == 'false') {
        var ups = require('int_ups/cartridge/scripts/services/UPS');
        args.CustomerAddressForm = session.forms.singleshipping.shippingAddress.addressFields;
        var validateAddressResult = ups.validateAddress(args);

        if (validateAddressResult === PIPELET_ERROR) {
            return false;
        } else {
            if (args.IsMultiAddress == 'true') {
                if (args.IsExactAddress) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    } else {
        return true;
    }
}

/*Module Exports*/
exports.ValidateAddress = validateAddress;
