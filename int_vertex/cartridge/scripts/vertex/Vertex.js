'use strict';

/* API includes */
var Transaction = require('dw/system/Transaction');

/* Script includes */
var CalculateOrderHash = require('~/cartridge/scripts/vertex/CalculateOrderHash');
var UpdateTax = require('~/cartridge/scripts/vertex/UpdateTax');
var VertexO = require('~/cartridge/scripts/vertex/VertexO');

function calculateTax(basket) {

    var args = {
        'Basket': basket
    };
    var vertexOResult;

    //Calculate tax only if shipping address exists in basket.
    if (!empty(basket) && !empty(basket.defaultShipment) && !empty(basket.defaultShipment.shippingAddress)) {

        //Get old order hash. It will be empty if it is first time. 
        var oldOrderHash = session.custom.TaxOrderHash;

        //Do order hashing only if it is enabled for the site. 
        var bypassOrderHash = 'vertexOEnableOrderHash' in dw.system.Site.getCurrent().preferences.custom ? !dw.system.Site.getCurrent().preferences.custom.vertexOEnableOrderHash : true;
        if (bypassOrderHash != null && bypassOrderHash == false) {

            //Calculate order hash.
            var calculateOrderHashResult;
            Transaction.wrap(function() {
                calculateOrderHashResult = CalculateOrderHash.calculateOrderHash(args);
            });

            //Update with previous values if calculate order hash errored out.
            if (calculateOrderHashResult === PIPELET_ERROR) {
                Transaction.wrap(function() {
                    UpdateTax.updateTax(args);
                });
                return false;
            }

            //Store new order hash in session.
            var newOrderHash = session.custom.TaxOrderHash;

            //Calculate tax only if there is a change in basket. Update with previous tax rates otherwise.  
            if (oldOrderHash === newOrderHash) {
                Transaction.wrap(function() {
                    UpdateTax.updateTax(args);
                });
                return true;
                
            } else {

                Transaction.wrap(function() {
                    vertexOResult = VertexO.vertexO(args);
                });

                //Update with previous values if rest call is errored out.
                if (vertexOResult === PIPELET_ERROR) {
                    Transaction.wrap(function() {
                        UpdateTax.updateTax(args);
                    });
                    return false;
                } else {
                    //Update with new values if rest call is successful.
                    Transaction.wrap(function() {
                        UpdateTax.updateTax(args);
                    });
                    return true;
                }

            }

        } else {

            //Call vertex every time we re-calculate basket if order hashing is not enabled.
            Transaction.wrap(function() {
                vertexOResult = VertexO.vertexO(args);
            });

            if (vertexOResult === PIPELET_ERROR) {
                Transaction.wrap(function() {
                    UpdateTax.updateTax(args);
                });
                return false;
            }
            
            Transaction.wrap(function() {
                UpdateTax.updateTax(args);
            });
            return true;
        }

    } else {
        //Update with default values(ZERO) if no shipping address exists in basket.
        Transaction.wrap(function() {
            UpdateTax.updateTax(args);
        });
        return true;
    }
}

exports.calculateTax = calculateTax;
