'use strict';

/**
 * Controller that manages the order history of a registered user.
 *
 * @module controllers/Order
 */

/* API Includes */
var ContentMgr = require('dw/content/ContentMgr');
var OrderMgr = require('dw/order/OrderMgr');
var PagingModel = require('dw/web/PagingModel');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');


/**
 * Renders a page with the order history of the current logged in customer.
 *
 * Creates a PagingModel for the orders with information from the httpParameterMap.
 * Invalidates and clears the orders.orderlist form. Updates the page metadata. Sets the
 * ContinueURL property to Order-Orders and renders the order history page (account/orderhistory/orders template).
 */
function history() {
	
	//JIRA PREV-57 : In Order History 'FAILED' orders are displaying. Excluding FAILED and CREATED status orders.  
    var orders = OrderMgr.searchOrders('customerNo={0} AND status!={1} AND status!={2} AND status!={3}', 'creationDate desc',
            customer.profile.customerNo, dw.order.Order.ORDER_STATUS_REPLACED, dw.order.Order.ORDER_STATUS_FAILED, dw.order.Order.ORDER_STATUS_CREATED);


    /*****************************PREVAIL - Order History Integration*****************************************/
    var count = orders.count;
    if (dw.system.Site.getCurrent().getCustomPreferenceValue('isEndToEndMerchant')) {
        var OMSOrders = getOrderHistory(orders);
        count = OMSOrders.size();
        orders = OMSOrders.iterator();
    }

    var parameterMap = request.httpParameterMap;
    var pageSize = parameterMap.sz.intValue || 5;
    var start = parameterMap.start.intValue || 0;
    var orderPagingModel = new PagingModel(orders, count);//PREVAIL - getting count before and using it here for the sake of OMS integration.
    orderPagingModel.setPageSize(pageSize);
    orderPagingModel.setStart(start);

    var orderListForm = app.getForm('orders.orderlist');
    orderListForm.invalidate();
    orderListForm.clear();
    orderListForm.copyFrom(orderPagingModel.pageElements);

    var pageMeta = require('~/cartridge/scripts/meta');
    pageMeta.update(ContentMgr.getContent('myaccount-orderhistory'));

    app.getView({
        OrderPagingModel: orderPagingModel,
        ContinueURL: dw.web.URLUtils.https('Order-Orders')
    }).render('account/orderhistory/orders');
}


/**
 * Gets an OrderView and renders the order detail page (account/orderhistory/orderdetails template). If there is an error,
 * redirects to the {@link module:controllers/Order~history|history} function.
 */
function orders() {
    var orderListForm = app.getForm('orders.orderlist');
    orderListForm.handleAction({
        show: function (formGroup, action) {
            var Order = action.object;

            /*****************************PREVAIL - Order Detail Integration*****************************************/
            if (dw.system.Site.getCurrent().getCustomPreferenceValue('isEndToEndMerchant')) {
                Order = getOrderDetail(Order);
            }

            app.getView({Order: Order}).render('account/orderhistory/orderdetails');
        },
        error: function () {
            response.redirect(dw.web.URLUtils.https('Order-History'));
        }
    });

}

/**
 * PREVAIL - Order history integration
 */
function getOrderHistory(orders) {
    var args = {
        DWOrders: orders
    };
    require('int_jde/cartridge/scripts/jde/OrderHistory').show(args);
    return args.OrdersUnpaged;
}

/**
 * PREVAIL - Order detail integration
 */
function getOrderDetail(order) {
    if (!(order instanceof dw.order.Order)) {
        var args = require('int_jde/cartridge/scripts/jde/OrderHistory').detail(order);
        return args.Order;
    } else {
        return order;
    }
}

/*
 * Web exposed methods
 */
/** Renders a page with the order history of the current logged in customer.
 * @see module:controllers/Order~history */
exports.History = guard.ensure(['get', 'https', 'loggedIn'], history);
/** Renders the order detail page.
 * @see module:controllers/Order~orders */
exports.Orders = guard.ensure(['post', 'https', 'loggedIn'], orders);
