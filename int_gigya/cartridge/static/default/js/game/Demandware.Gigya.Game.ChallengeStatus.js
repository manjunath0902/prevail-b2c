/*******************************************************************
 * 
 *  Invokes the game mechanics challenge status plugin
 * 
 ******************************************************************/

if(typeof(Demandware)=='undefined')Demandware = {};
if(!Demandware.Gigya)Demandware.Gigya = {};
if(!Demandware.Gigya.Game)Demandware.Gigya.Game = {};

Demandware.Gigya.Game.ChallengeStatus = new function(){
	var _divParams = null;
	var _width = null;
	var _challenge = null;
	var _additionalParameters = null;
		
	//constructor
	new function(){
		$(document).bind("ready", initialize);
		$(window).bind("unload", dispose);
	}
	
	function initialize(){
		initializeElements();
		showChallengeStatus();
	}
	
	function initializeElements(){
		_divParams = $('#divGigyaChallengeStatusParams');
		_width = _divParams.find('#txtGigyaChallengeStatusWidth').val()||300;
		_challenge =  _divParams.find('#txtGigyaChallengeStatusChallenge').val()||'_default';	
		if ( _divParams.find('#txtGigyaAdditionalParameters').val() !== "null") {
			_additionalParameters = $.parseJSON(_divParams.find('#txtGigyaAdditionalParameters').val());
		}
	}
	
	function showChallengeStatus(){
		var statusParams = {
			containerID:'divGigyaChallengeStatus'
			,width:_width
			,challenge:_challenge
		};
		
		if ( _divParams.find('#txtGigyaAdditionalParameters').val() !== "null") {
			for (var property in _additionalParameters) {
				statusParams[property] = _additionalParameters[property];
			}
		}
		
		gigya.gm.showChallengeStatusUI(statusParams);
	}
	
	function dispose(){
		_divParams = null;
	}
}