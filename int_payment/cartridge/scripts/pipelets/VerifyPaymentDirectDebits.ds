/**
* Validate the Direct Debits forms data fields.
* The script get the PaymentForms object at input, containing all forms data (Direct Debits DE, AT and NL)
* , validate current form and add output results: - the status of the validation, - the paymentType and - the data of the current form validated.    
*
* @input PaymentForms : Object
* @input Customer: dw.customer.Customer
* @output DirectDebitsStatus : dw.system.Status
* @output PaymentType : String
* @output DirectDebitsForm : Object
*
*/
importPackage( dw.system );
importPackage( dw.web );

importScript("int_payment:lib/libPayment.ds");

function execute( args : PipelineDictionary ) : Number {
	var forms = args.PaymentForms;
	var paymentType : String = forms.selectedPaymentMethodID.htmlValue;
	var newSEPA = Site.getCurrent().getCustomPreferenceValue('enableNewSEPAVersion');
	
	var customer : Customer = args.Customer ;

	if (empty(paymentType)) {
		Logger.error('VerifyPaymentDirectDebits.ds : Empty paymentType');
		return PIPELET_ERROR;
	}

	var mapForms = {
		'DIRECT_DEBITS_DE' : forms.directdebitsde,
		'DIRECT_DEBITS_NL' : forms.directdebitsnl,
		'DIRECT_DEBITS_AT' : forms.directdebitsat
	};

	var form = mapForms[paymentType];
	var status : dw.system.Status = new dw.system.Status();

	// Validate Direct Debits DE form:
	if (paymentType == 'DIRECT_DEBITS_DE') {
		// Check to be completed the Konto and Blz fields, if Iban is empty (CARDNO = Konto + Blz) or Iban completed (CARDNO = Iban).
		// Not allowed IBAN with Konto or Blz empty, or all three empty.
		var ibanRequired = newSEPA && empty(form.iban.htmlValue);
		var ibanNullOrRequired = !newSEPA || ibanRequired;
		if (ibanRequired && empty(form.konto.htmlValue) && empty(form.blz.htmlValue)) {
			var statusItem = new StatusItem(
				Status.ERROR,
				'IBAN-MISSING',
				Resource.msg('forms.directdebitsde.iban.missing', 'forms', null),
				{});
			status.addItem(statusItem);
		} else if (ibanNullOrRequired && empty(form.konto.htmlValue)) {
			var statusItem = new StatusItem(
				Status.ERROR,
				'KONTO-MISSING',
				Resource.msg('forms.directdebitsde.konto.missing', 'forms', null),
				{});
			status.addItem(statusItem);
		} else if (ibanNullOrRequired && empty(form.blz.htmlValue)) {
			var statusItem = new StatusItem(
				Status.ERROR,
				'BLZ-MISSING',
				Resource.msg('forms.directdebitsde.blz.missing', 'forms', null),
				{});
			status.addItem(statusItem);
		} else {
			// Check the fields data lengths: iban=22 digits, konto=up to 10, blz=8 digits 
			if (newSEPA && !empty(form.iban.htmlValue)) {
				if (form.iban.htmlValue.replace(/\s/g, '').length != 22) {
					var statusItem = new StatusItem(
						Status.ERROR,
						'IBAN-INVALID',
						Resource.msg('forms.directdebitsde.iban.invalid', 'forms', null),
						{});
					status.addItem(statusItem);
				}
			}
			if (!empty(form.konto.htmlValue)) {
				if (form.konto.htmlValue.replace(/\s/g, '').length > 10) {
					var statusItem = new StatusItem(
						Status.ERROR,
						'KONTO-INVALID',
						Resource.msg('forms.directdebitsde.konto.invalid', 'forms', null),
						{});
					status.addItem(statusItem);
				}
			}
			if (!empty(form.blz.htmlValue)) {
				if (form.blz.htmlValue.replace(/\s/g, '').length != 8) {
					var statusItem = new StatusItem(
						Status.ERROR,
						'BLZ-INVALID',
						Resource.msg('forms.directdebitsde.blz.invalid', 'forms', null),
						{});
					status.addItem(statusItem);
				}
			}
		}
		// - Expiration Date fields
		if (empty(form.month.htmlValue) || empty(form.year.htmlValue)) {
			var statusItem = new StatusItem(
				Status.ERROR,
				'ED-MISSING',
				Resource.msg('forms.directdebitsnl.ed.missing', 'forms', null),
				{});
			status.addItem(statusItem);
		} else {
			var ccExpYear = new Number(form.year.selectedOption.value);
			var ccExpMonth = new Number(form.month.selectedOption.value) - 1; // January should be 0

			var today = new Date();
			var expDate = new Date(ccExpYear, ccExpMonth, 1);

			if (expDate < today) {
				var statusItem = new StatusItem(
					Status.ERROR,
					'ED-INVALID',
					Resource.msg('forms.directdebitsnl.ed.invalid', 'forms', null),
					{});
				status.addItem(statusItem);
			}
		}
	// Validate Direct Debits AT form:
	} else if (paymentType == 'DIRECT_DEBITS_AT') {
		// - konto field (lengths for Austria: konto=up to 11 digits, blz=5 digits)
		if (empty(form.konto.htmlValue)) {
			var statusItem = new StatusItem(
				Status.ERROR,
				'KONTO-MISSING',
				Resource.msg('forms.directdebitsat.konto.missing', 'forms', null),
				{});
			status.addItem(statusItem);
		} else if (form.konto.htmlValue.replace(/\s/g, '').length > 11) {
			var statusItem = new StatusItem(
				Status.ERROR,
				'KONTO-INVALID',
				Resource.msg('forms.directdebitsat.konto.invalid', 'forms', null),
				{});
			status.addItem(statusItem);
		}
		// - blz field
		if (empty(form.blz.htmlValue)) {
			var statusItem = new StatusItem(
				Status.ERROR,
				'BLZ-MISSING',
				Resource.msg('forms.directdebitsat.blz.missing', 'forms', null),
				{});
			status.addItem(statusItem);
		} else if (form.blz.htmlValue.replace(/\s/g, '').length != 5) {
			var statusItem = new StatusItem(
				Status.ERROR,
				'BLZ-INVALID',
				Resource.msg('forms.directdebitsat.blz.invalid', 'forms', null),
				{});
			status.addItem(statusItem);
		}
		// - Expiration Date fields
		if (empty(form.month.htmlValue) || empty(form.year.htmlValue)) {
			var statusItem = new StatusItem(
				Status.ERROR,
				'ED-MISSING',
				Resource.msg('forms.directdebitsnl.ed.missing', 'forms', null),
				{});
			status.addItem(statusItem);
		} else {
			var ccExpYear = new Number(form.year.selectedOption.value);
			var ccExpMonth = new Number(form.month.selectedOption.value) - 1; // January should be 0

			var today = new Date();
			var expDate = new Date(ccExpYear, ccExpMonth, 1);

			if (expDate < today) {
				var statusItem = new StatusItem(
					Status.ERROR,
					'ED-INVALID',
					Resource.msg('forms.directdebitsnl.ed.invalid', 'forms', null),
					{});
				status.addItem(statusItem);
			}
		}
	// Validate Direct Debits NL form:
	} else if (paymentType == 'DIRECT_DEBITS_NL') {
		// - IBAN field (lengths for Netherland: Iban=18 digits, bic=up to 11 digits)
		if (empty(form.iban.htmlValue)) {
			var statusItem = new StatusItem(
				Status.ERROR,
				'IBAN-MISSING',
				Resource.msg('forms.directdebitsnl.iban.missing', 'forms', null),
				{});
			status.addItem(statusItem);
		}
		// - BIC field if SEPA enabled
		if (newSEPA && !empty(form.bic.htmlValue)) {
			if (form.bic.htmlValue.replace(/^\s+|\s+$/gm,'').length > 11) {
				var statusItem = new StatusItem(
					Status.ERROR,
					'BIC-INVALID',
					Resource.msg('forms.directdebitsnl.bic.invalid', 'forms', null),
					{});
				status.addItem(statusItem);
			}
		}
	}

	// For all Direct Debits forms 
	// - Cardholder Name field
	if (empty(form.cn.htmlValue)) {
		var statusItem = new StatusItem(
			Status.ERROR,
			'CN-MISSING',
			Resource.msg('forms.directdebitsnl.cn.missing', 'forms', null),
			{});
		status.addItem(statusItem);
	}

	args.DirectDebitsStatus = status;
	args.DirectDebitsForm = form;
	args.PaymentType = paymentType;
	
	if (status.error) {
		return PIPELET_ERROR;
	}
	
	
	var checkValues = form.cn.htmlValue;
	if (paymentType != 'DIRECT_DEBITS_NL') {
		checkValues += "," + form.month.selectedOption.value + "," + form.year.selectedOption.value;
	} 
	

	return PIPELET_NEXT;
}
