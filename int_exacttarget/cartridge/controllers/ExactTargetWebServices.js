'use strict';

/**
 * Controller that used to trigger email's to customers.
 *  @module controllers/ExactTargetWebServices
 */

function ETWSError() {
    return;
}

function createAndSendTriggeredSend(options) {
    var createAndSendTrigger = require('~/cartridge/scripts/pipelets/CreateAndSendTriggeredSend');
    var result = createAndSendTrigger.create(options);
    if (result.error) {
        ETWSError();
    }
}

exports.createAndSendTriggeredSend = createAndSendTriggeredSend;
