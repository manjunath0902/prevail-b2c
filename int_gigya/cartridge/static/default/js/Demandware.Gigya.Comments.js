/*******************************************************************
 * 
 *  Invokes the Comments plugin
 * 
 ******************************************************************/

if(typeof(Demandware)=='undefined')Demandware = {};
if(!Demandware.Gigya)Demandware.Gigya = {};

Demandware.Gigya.Comments = new function(){
	var _divParams = null;
	var _width = null;
	var _categoryId = null;
	var _streamId = null;
	var _feedId = null;
	var _shareWithFeed = null;
	var _additionalParameters = null;
			
	//constructor
	new function(){
		$(document).bind("ready", initialize);
		$(window).bind("unload", dispose);
	}
	
	function initialize(){
		initializeElements();
		showComments();
	}
	
	function initializeElements(){
		_divParams = $('#divGigyaCommentsParams');
		_width=_divParams.find('#txtGigyaCommentsWidth').val()||500;
		_categoryId = _divParams.find('#txtGigyaCommentsCategoryId').val();
		_streamId = _divParams.find('#txtGigyaCommentsStreamId').val();
		_feedId = _divParams.find('#txtGigyaCommentsFeedId').val();
		_shareWithFeed = _divParams.find('#txtGigyaCommentsFeed').val()!='false';
		if ( _divParams.find('#txtGigyaAdditionalParameters').val() !== "null") {
			_additionalParameters = $.parseJSON(_divParams.find('#txtGigyaAdditionalParameters').val());
		}
	}
	
	function showComments(){
		var action = new gigya.services.socialize.UserAction();
		action.setActionName('commented');
		
		var commentsParams = {
				containerID:'divGigyaComments'
				,categoryID:_categoryId
				,streamID:_streamId
				,version: 2
				,showLoginBar: true
				,userAction : action
				,context : action
				,scope:_shareWithFeed?'both':'external'
				,privacy:Demandware.Gigya.PrivacyLevel
		};
		if ( _divParams.find('#txtGigyaAdditionalParameters').val() !== "null") {
			for (var property in _additionalParameters) {
				commentsParams[property] = _additionalParameters[property];
			}
		}
		commentsParams.onBeforeCommentSubmitted = commentSubmiting; 
		gigya.services.comments.showCommentsUI(commentsParams);
		
	}
	
	function commentSubmiting(eventData){
		var action = eventData.context;
		action.setTitle(eventData.commentText);
		action.setLinkBack(window.location.href);
	}
	
	function dispose(){
		_divParams = null;
	}
}