/*******************************************************************
 * 
 *  Invokes the Ratings & Comments plugin
 * 
 ******************************************************************/

if(typeof(Demandware)=='undefined')Demandware = {};
if(!Demandware.Gigya)Demandware.Gigya = {};

Demandware.Gigya.Ratings = new function(){
	var _this = this;
	var _divParams = null;
	var _categoryId = null;
	var _description = null;
	var _streamId = null;
	var _showComments = null;
	var _commentsId = null;
	var _feedId = null;
	var _shareWithFeed = null;
	var _imageUrl = null;
	var _additionalParameters = null;
	
	_this.OnAddReviewClicked = null;
	_this.OnWriteReviewClicked = null;
	
		
	//constructor
	new function(){
		$(document).bind("ready", initialize);
		$(window).bind("unload", dispose);
	}
	
	function initialize(){
		initializeElements();
		showRatings();
	}
	
	function initializeElements(){
		_divParams = $('#divGigyaRatingsParams');
		_categoryId = _divParams.find('#txtGigyaRatingsCategoryId').val();
		_description = _divParams.find('#txtGigyaRatingsDescription').val();
		_streamId = _divParams.find('#txtGigyaRatingsStreamId').val();
		_showComments = _divParams.find('#txtGigyaRatingsShowComments').val().toLowerCase()=='true';
		_feedId = _divParams.find('#txtGigyaRatingsFeedId').val();
		_commentsId = _divParams.find('#txtGigyaRatingsCommentsId').val();
		_shareWithFeed = _divParams.find('#txtGigyaRatingsFeed').val()!='false';
		_imageUrl = _divParams.find('#txtGigyaRatingsImageUrl').val();
		if ( _divParams.find('#txtGigyaAdditionalParameters').val() !== "null") {
			_additionalParameters = $.parseJSON(_divParams.find('#txtGigyaAdditionalParameters').val());
		}
	}
	
	function showRatings(){
		var ratingsParams = {
			containerID:'divGigyaRatings'
			,categoryID:_categoryId
			,streamID:_streamId
		};
		if ( _divParams.find('#txtGigyaAdditionalParameters').val() !== "null") {
			for (var property in _additionalParameters) {
				ratingsParams[property] = _additionalParameters[property];
			}
		}
		setupReviewEvents(ratingsParams);
		setupComments(ratingsParams);					
		gigya.services.socialize.showRatingUI(ratingsParams);
		
	}
	
	function setupReviewEvents(ratingsParams){
		if(typeof(_this.OnAddReviewClicked=='function'))
			ratingsParams.onAddReviewClicked = _this.OnAddReviewClicked; 
		if(typeof(_this.OnReadReviewsClicked=='function'))
				ratingsParams.onReadReviewsClicked = _this.OnReadReviewsClicked;
	}
	
	function setupComments(ratingsParams){
		if(!_showComments) return;
		
		var commentContainer = _commentsId||'divGigyaRatingsComments';
		ratingsParams.linkedCommentsUI = commentContainer 
		
		var commentsParams = {
				containerID:commentContainer
				,categoryID:_categoryId
				,streamID:_streamId
				,version: 2
				,scope:_shareWithFeed?'both':'external'
				,privacy:Demandware.Gigya.PrivacyLevel
				,onBeforeCommentSubmitted:commentCallback
		};
				
		var action = new gigya.services.socialize.UserAction();
		if(_imageUrl)
			action.addMediaItem( { type: "image", src: _imageUrl, href: location.href });
		          
		commentsParams.context = action;
		commentsParams.userAction = action;
				
		gigya.services.comments.showCommentsUI(commentsParams);
	}
	
	function commentCallback(eventData){
		var action = eventData.context;
		action.setActionName('rated');
		action.setTitle(eventData.commentTitle);
		if(_description)
			action.setDescription(_description);
		action.setLinkBack(location.href);
	}
	
	function dispose(){
		_this = _divParams = _this.OnAddReviewClicked = _this.OnWriteReviewClicked = null
	}
}