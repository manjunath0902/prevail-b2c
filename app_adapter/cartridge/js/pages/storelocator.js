'use strict';
var dialog = require('../dialog');

exports.init = function() {
    $('.store-details-link').on('click', function(e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href')
        });
    });

    //PREVAIL - Added below block for Google maps integration.
    if (isPREVAILStoreLocatorEnabled) {
        /* "storeMap" is a global function that will be passed as callback function for google maps API. See below
         * <script src="//maps.googleapis.com/maps/api/js?callback=storeMap" async defer></script> 
         */
        window.storeMap = function() {

            //Create map
            var mapCanvas = document.getElementById('map');
            var mapOptions = {
                center: new google.maps.LatLng(37.09024, -95.712891),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(mapCanvas, mapOptions);

            var stores = $('.storeJSON').data('storejson'),
                markers = [],
                bounds = new google.maps.LatLngBounds(),
                store, marker, storeCenter, markerContent, mapLink,
                storeName, storePhone, storAddress1, storeCity, storeStateCode, storePostalCode, storeEmail, storeCountryCode;

            //Create markers and info window and add them to map.
            if (stores.length > 0) {
                for (var index in stores) {
                    store = stores[index];
                    if (!store.latitude || !store.longitude) {
                        continue;
                    }
                    storeCenter = new google.maps.LatLng(store.latitude.toFixed(6), store.longitude.toFixed(6));
                    storeName = store.name ? store.name : '';
                    storePhone = store.phone ? store.phone : '';
                    storAddress1 = store.address1 ? store.address1 : '';
                    storeCity = store.city ? store.city : '';
                    storeStateCode = store.stateCode ? store.stateCode : '';
                    storePostalCode = store.postalCode ? store.postalCode : '';
                    storeEmail = store.email ? store.email : '';
                    storeCountryCode = store.countryCode ? store.countryCode : '';

                    mapLink = 'http://maps.google.com/maps?hl=en&f=q&q=' +
                        storAddress1 +
                        ',' + storeCity +
                        ',' + storeStateCode +
                        ',' + storePostalCode +
                        ',' + storeCountryCode;

                    markerContent = '<div class="store-infowindow">' +
                        '<h2>' + storeName + '</h2>' +
                        '<p>' + storAddress1 + '<br/>' + storeCity + ',' + storeStateCode + storePostalCode + '</p>' +
                        '<div class="storephone">' + storePhone + '<br/>' + storeEmail + '</div>' +
                        '<div class="storemap"><a class="googlemap" href="' + mapLink + '" target="_blank">View on larger map</a></div>' + '</div>';

                    marker = new google.maps.Marker({
                        position: storeCenter,
                        title: store.name,
                        content: markerContent
                    });

                    bounds.extend(storeCenter);
                    marker.setMap(map);
                    markers.push(marker);
                }

                var infowindow = new google.maps.InfoWindow({
                    content: ''
                });

                for (var i = 0; i < markers.length; i++) {
                    marker = markers[i];
                    marker.index = i;
                    google.maps.event.addListener(marker, 'click', function() {
                        infowindow.setContent(this.content);
                        map.setZoom(9);
                        infowindow.open(map, this);
                    });
                }
            }

            map.fitBounds(bounds);

            $('.store-click').each(function(i) {
                jQuery(this).bind('click', function(e) {
                    e.preventDefault();
                    google.maps.event.trigger(markers[i], 'click');
                });
            });
        }
    }
};
