'use strict';

/**
 * Controller for the first step of the cart checkout process, which is to ask the customer to login, register, or
 * checkout anonymously.
 *
 * @module controllers/COCustomer
 */

/* API Includes */
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');

var Cart = require('~/cartridge/scripts/models/CartModel');
var Content = require('~/cartridge/scripts/models/ContentModel');

/**
 * First step of the checkout is to choose the checkout type: returning, guest or create account checkout.
 * Prepares the checkout initially: removes all payment instruments from the basket and clears all
 * forms used in the checkout process, when the customer enters the checkout. The single steps (shipping, billing etc.)
 * may not contain the form clearing, in order to support navigating forth and back in the checkout steps without losing
 * already entered form values.
 */
function start() {
    var oauthLoginForm = app.getForm('oauthlogin');
    app.getForm('singleshipping').clear();
    app.getForm('multishipping').clear();
    app.getForm('billing').clear();

    Transaction.wrap(function () {
        Cart.goc().removeAllPaymentInstruments();
    });

    /*****************************PREVAIL - Real-time inventory check*****************************************/
    var result = realTimeInventoryCheck();
    if (!result) {
        app.getController('Cart').Show();
        return;
    }

    /*PREVAIL - JIRA PREV-45 : Able to proceed to checkout flow with more than instock qty by clicking on Go straight to checkout in the Mini cart.
      Added below block.*/
    if (!Cart.goc().validateForCheckout().EnableCheckout) {
        app.getController('Cart').Show();
        return;
    }
    
    /*****************************PREVAIL - Amazon Payments Integration*****************************************/
    if (dw.system.Site.getCurrent().getCustomPreferenceValue('isPayWithAmazonEnabled')) {
        require('int_amazonpayments/cartridge/controllers/AmazonPayment').initAmazonCheckout();
        if (session.privacy.isInAmazonPayment) {
            response.redirect(URLUtils.https('COShipping-Start'));
        }
    }
    
    // Direct to first checkout step if already authenticated.
    if (customer.authenticated) {
        response.redirect(URLUtils.https('COShipping-Start'));
        return;
    } else {
        var loginForm = app.getForm('login');
        loginForm.clear();
        oauthLoginForm.clear();

        // Prepopulate login form field with customer's login name.
        if (customer.registered) {
            loginForm.setValue('username', customer.profile.credentials.login);
        }

        var loginAsset = Content.get('myaccount-login');

        var pageMeta = require('~/cartridge/scripts/meta');
        pageMeta.update(loginAsset);

        app.getView({
            Basket: Cart.goc().object,//PREVAIL - Passing missing pdict variable Basket
            ContinueURL: URLUtils.https('COCustomer-LoginForm').append('scope', 'checkout')
        }).render('checkout/checkoutlogin');
    }

}

/**
 * Form handler for the login form. Handles the following actions:
 * - __login__ - Calls the {@link module:controllers/Login~process|Login controller Process function}. If this returns successfully, calls
 * the {@link module:controllers/COShipping~Start|COShipping controller Start function}.
 * - __register__ - Calls the {@link module:controllers/Account~StartRegister|Account controller StartRegister function}.
 * - __unregistered__ - Calls the {@link module:controllers/COShipping~Start|COShipping controller Start function}.
 */
function showLoginForm() {
    var loginForm = app.getForm('login');

    /************************************PREVAIL - SPC Customization*****************************************/
    if (dw.system.Site.getCurrent().getCustomPreferenceValue('checkoutTemplateType').toString() === 'ONE_PAGE') {
        session.custom.TargetLocation = URLUtils.https('COShipping-Start', 'login', 'true').toString();
    } else {
        //PREVAIL - Added .toString() to fix deprecated API issue.
        session.custom.TargetLocation = URLUtils.https('COShipping-Start').toString();
    }

    loginForm.handleAction({
        login: function () {
            app.getController('Login').LoginForm();
        },
        register: function () {
            /************************************PREVAIL - SPC Customization*****************************************/
            if (dw.system.Site.getCurrent().getCustomPreferenceValue('checkoutTemplateType').toString() === 'ONE_PAGE') {
                session.custom.TargetLocation = URLUtils.https('COShipping-Start', 'registration', 'true').toString();
            }
            response.redirect(URLUtils.https('Account-StartRegister'));
        },
        unregistered: function () {

            /*****************************PREVAIL - SPC Customization*****************************************/
            if (dw.system.Site.getCurrent().getCustomPreferenceValue('checkoutTemplateType').toString() === 'ONE_PAGE') {
                response.redirect(URLUtils.https('COShipping-Start', 'login', 'true'));
            } else {
                response.redirect(URLUtils.https('COShipping-Start'));
            }
        }
    });
}


/**
 * PREVAIL - Inventory check
 */
function realTimeInventoryCheck() {
    if (dw.system.Site.getCurrent().getCustomPreferenceValue('enableRealTimeInventoryCheck') ||
        dw.system.Site.getCurrent().getCustomPreferenceValue('isEndToEndMerchant')) {
        return require('int_inventorycheck/cartridge/scripts/inventorycheck/RealTimeInventory').check();
    }
    return true;
}

/**
 * PREVAIL - SPC
 */
function backToSignIn() {

    app.getForm('singleshipping').clear();
    app.getForm('multishipping').clear();
    app.getForm('billing').clear();

    Transaction.wrap(function () {
        Cart.goc().removeAllPaymentInstruments();
    });


    /*****************************PREVAIL - Real-time inventory check*****************************************/
    var pdict = realTimeInventoryCheck();
    if (pdict && pdict.EndNodeName === 'ERROR') {
        app.getController('Cart').Show();
        return;
    }

    var loginForm = app.getForm('login');
    loginForm.clear();

    // Prepopulate login form field with customer's login name.
    if (customer.registered) {
        loginForm.setValue('username', customer.profile.credentials.login);
    }

    var loginAsset = Content.get('myaccount-login');

    var pageMeta = require('~/cartridge/scripts/meta');
    pageMeta.update(loginAsset);

    app.getView({
        Basket: Cart.goc().object,
        ContinueURL: URLUtils.https('COCustomer-LoginForm')
    }).render('checkout/checkoutlogin');
}

/*
 * Module exports
 */

/*
 * Web exposed methods
 */
/** Selects the type of checkout: returning, guest, or create account. The first step in the checkout process.
 * @see module:controllers/COCustomer~start */
exports.Start = guard.ensure(['https'], start);
/** Form handler for the login form.
 * @see module:controllers/COCustomer~showLoginForm */
exports.LoginForm = guard.ensure(['https', 'post', 'csrf'], showLoginForm);
/**
 * PREVAIL - @see module:controllers/COCustomer~BackToSignIn*/
exports.BackToSignIn = guard.ensure(['https'], backToSignIn);
