/*******************************************************************
 * 
 *  Invokes the game mechanics user status plugin
 * 
 ******************************************************************/

if(typeof(Demandware)=='undefined')Demandware = {};
if(!Demandware.Gigya)Demandware.Gigya = {};
if(!Demandware.Gigya.Game)Demandware.Gigya.Game = {};

Demandware.Gigya.Game.UserStatus = new function(){
	var _divParams = null;
	var _width = null;
	var _challenge = null;
	var _additionalParameters = null;
		
	//constructor
	new function(){
		$(document).bind("ready", initialize);
		$(window).bind("unload", dispose);
	}
	
	function initialize(){
		initializeElements();
		showUserStatus();
	}
	
	function initializeElements(){
		_divParams = $('#divGigyaUserStatusParams');
		_width = _divParams.find('#txtGigyaUserStatusWidth').val()||300;
		_challenge =  _divParams.find('#txtGigyaUserStatusChallenge').val()||'_default';	
		if ( _divParams.find('#txtGigyaAdditionalParameters').val() !== "null") {
			_additionalParameters = $.parseJSON(_divParams.find('#txtGigyaAdditionalParameters').val());
		}
	}
	
	function showUserStatus(){
		var statusParams = {
			containerID:'divGigyaUserStatus'
			,width:_width
			,challenge:_challenge
		};
		
		if ( _divParams.find('#txtGigyaAdditionalParameters').val() !== "null") {
			for (var property in _additionalParameters) {
				statusParams[property] = _additionalParameters[property];
			}
		}
		
		gigya.gm.showUserStatusUI(statusParams);
	}
	
	function dispose(){
		_divParams = null;
	}
}