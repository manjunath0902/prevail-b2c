/*******************************************************************
 * 
 *  Invokes the Reactions bar plugin
 * 
 ******************************************************************/

if(typeof(Demandware)=='undefined')Demandware = {};
if(!Demandware.Gigya)Demandware.Gigya = {};

Demandware.Gigya.Reactions = new function(){
	var _this = this;
	var _divParams = null;
	var _reactionId = null;
	var _showSuccess = null;
	var _buttonBorders = null;
	var _allowMultiple = null;
	var _countType = null;
	var _layout = null;
	var _counts = null;
	var _linkUrl = null;
	var _imageUrl = null;
	var _title = null;
	var _shareWithFeed = null;
	var _reactionOptions = [];
	var _additionalParameters = null;
		
	//constructor
	new function(){
		$(document).bind("ready", initialize);
		$(window).bind("unload", dispose);
	}
	
	function initialize(){
		initializeElements();
		showReactionBar();
	}
	
	function initializeElements(){
		_divParams = $('#divGigyaReactionsParams');
		_reactionId = _divParams.find('#txtGigyaReactionsId').val();
		_showSuccess = _divParams.find('#txtGigyaReactionsShowSuccess').val()||'true';
		_buttonBorders = _divParams.find('#txtGigyaReactionsButtonBorders').val()||'true';
		_allowMultiple = _divParams.find('#txtGigyaReactionsAllowMultiple').val();
		_countType = _divParams.find('#txtGigyaReactionsCountType').val()||'number';
		_layout = _divParams.find('#txtGigyaReactionsLayout').val()||'horizontal';
		_counts = _divParams.find('#txtGigyaReactionsCounts').val()||'right';
		_reactionOptions = eval(_divParams.find('#txtGigyaReactionsJSON').val());
		_linkUrl = _divParams.find('#txtGigyaReactionsLinkUrl').val()||window.location.href;
		_imageUrl = _divParams.find('#txtGigyaReactionsImageUrl').val();
		_title= _divParams.find('#txtGigyaReactionsTitle').val();
		_shareWithFeed = _divParams.find('#txtGigyaReactionsFeed').val()!='false';
		if ( _divParams.find('#txtGigyaAdditionalParameters').val() !== "null") {
			_additionalParameters = $.parseJSON(_divParams.find('#txtGigyaAdditionalParameters').val());
		}
	}
	
	function showReactionBar(){
		var action = new gigya.services.socialize.UserAction();
		
		if(_title)
			action.setTitle(_title);
		if(_linkUrl)
			action.setLinkBack(_linkUrl);
		if(_imageUrl)
			action.addMediaItem( { type: "image", src: _imageUrl, href: _linkUrl }); 
				
		var reactionsParams =
	    { 
	    	barID: _reactionId
	    	,showCounts: _counts
	    	,containerID: "divGigyaReactions"
	    	,reactions: _reactionOptions
	    	,userAction: action
	    	,context: action //object to be referenced after event
	    	,showSuccessMessage: _showSuccess
	    	,noButtonBorders: !(_buttonBorders=='true')
	    	,multipleReactions:_allowMultiple
	    	,countType:_countType
	    	,layout:_layout
	    	,scope:_shareWithFeed?'both':'external'
    		,privacy:Demandware.Gigya.PrivacyLevel
    	}
		if ( _divParams.find('#txtGigyaAdditionalParameters').val() !== "null") {
			for (var property in _additionalParameters) {
				reactionsParams[property] = _additionalParameters[property];
			}
		}
        reactionsParams.onReactionClicked=reactionClicked;
        gigya.services.socialize.showReactionsBarUI(reactionsParams);
	}
	
	function reactionClicked(eventData){
		var reaction = eventData.reaction;
		var action = eventData.context;
		action.setActionName(reaction.feedMessage);
		action.setTitle(_title);
	}
	
	function dispose(){
		_this = _divParams = null;
	}
}